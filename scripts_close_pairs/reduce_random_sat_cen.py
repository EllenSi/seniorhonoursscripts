from astropy.io import fits, ascii
import numpy as np
import sys
from astropy.table import Table, Column
import random

# galtype = sys.argv[1]
# lines of sight = sys.argv[2]

with open('../Sim_Data/close_pairs_'+sys.argv[1]+'_15_'+sys.argv[2]+'.txt') as f:
    data = f.read()

f.close()

data = data.split('\n')

gal1_array = []
gal2_array = []

for row in data[:-1]:
    gal_one, gal_two = (float(x.strip("(),")) for x in row.split())
    gal1_array.append(gal_one)
    gal2_array.append(gal_two)


with open('../Sim_Data/'+sys.argv[1]+'_'+sys.argv[2]+'.asc') as f:
    data = f.read()

f.close()

data = data.split('\n')
data_new = data[:-1]

data_list = []
properties_list = []

for i in data_new:
    data_list.append(i.split(' '))

for j in range(0, len(data_list)):
    properties_list.append([float(l) for l in data_list[j]])

x = []
y = []
s1 = []
s2 = []
z_s = []
z_p = []

for i in range(0,len(properties_list)):
    
            x.append(properties_list[i][0])     
            y.append(properties_list[i][1])
            s1.append(properties_list[i][3])
            s2.append(properties_list[i][4])
            z_p.append(properties_list[i][5])
            z_s.append(properties_list[i][6])

x = np.array(x)
y = np.array(y)
s1 = np.array(s1)
s2 = np.array(s2)
z_s = np.array(z_s)
z_p = np.array(z_p)

mask = np.ones(len(x), dtype=bool)

print(len(gal1_array))
for i in range(0,len(gal1_array)):

    if i%1000 == 0:
        frac = float(i)/float(len(gal1_array))
        print(i,frac)
        
    gal1 = int(gal1_array[i])
    gal2 = int(gal2_array[i])

    if mask[gal1] == True and mask[gal2] == True:

        nr = random.random()
        
        if nr <= 0.5:

        	mask[gal1] = False
                                                
        else:
        	mask[gal2] = False
            
x_red = x[mask,...]
y_red = y[mask,...]
s1_red = s1[mask,...]
s2_red = s2[mask,...]
z_s_red = z_s[mask,...]
z_p_red = z_p[mask,...]

print('Writing tables...')
            
ascii.write([x_red, y_red,s1_red,s2_red, z_s_red,z_p_red], '../Sim_Data/reduced/'+sys.argv[1]+'_reduced_random_'+sys.argv[2]+'.asc', names=['x','y','s1','s2','z_s','z_p'])

new_col1 = fits.ColDefs([fits.Column(name='x_arcmin', format='D',array=np.array(x_red))])
new_col2 = fits.ColDefs([fits.Column(name='y_arcmin', format='D',array=np.array(y_red))])
new_col3 = fits.ColDefs([fits.Column(name='shear1', format='D',array=np.array(s1_red))])
new_col4 = fits.ColDefs([fits.Column(name='shear2', format='D',array=np.array(s2_red))])
hdu = fits.BinTableHDU.from_columns(new_col1 + new_col2 + new_col3 + new_col4)
hdu.writeto('../Sim_Data/reduced/'+sys.argv[1]+'_reduced_random_'+sys.argv[2]+'.fits')

        
