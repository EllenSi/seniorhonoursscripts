from astropy.io import fits, ascii
from scipy.interpolate import interp1d

import matplotlib.pyplot as plt
import numpy as np

# galtype = sys.argv[1]
# cuts = sys.argv[2]
# line sof sight = sys.argv[3]


hdulist = fits.open('../Sim_Data/xi_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.fits')
tbdata = hdulist[1].data

theta_athena = tbdata.field(0)
xip_athena = tbdata.field(1)
xim_athena = tbdata.field(2)

data = ascii.read('../Sim_Data/xi_m_nic_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.txt', delimiter = ' ')

theta_xi_m_nic = data['col1']
xi_m_nic = data['col2']

f_xi_m_nic = interp1d(theta_xi_m_nic, xi_m_nic)

theta_xi_m_ath_short = []
xi_m_ath_short = []

for i in range(0, len(theta_athena)):
	if theta_athena[i] > min(theta_xi_m_nic):
		theta_xi_m_ath_short.append(theta_athena[i]) 
		xi_m_ath_short.append(xim_athena[i])

fig = plt.figure()

plt.loglog(theta_xi_m_nic, xi_m_nic, 'o', theta_xi_m_ath_short, f_xi_m_nic(theta_xi_m_ath_short), 'ro')
plt.legend(['nic', 'nic_int'], loc='best')
plt.xlabel(r'$\theta$ [arcmin]')
plt.ylabel(r'$\xi_-$')
plt.title(r'nicaea $\xi_-$ vs. $\theta$ interpolated')
plt.xlim( min(theta_xi_m_nic), max(theta_xi_m_nic))
plt.tight_layout()
plt.grid(True)
plt.show()

fig.savefig('../Plots/pdf/intp1d_xi_m_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.pdf')
fig.savefig('../Plots/png/intp1d_xi_m_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.png')

xi_m_nic_interpld = f_xi_m_nic(theta_xi_m_ath_short)
xi_m_frac = []

for i in range(0, len(xi_m_ath_short)):
	xi_m_frac.append((xi_m_ath_short[i]/xi_m_nic_interpld[i]) - 1)

fig = plt.figure()

plt.plot(theta_xi_m_ath_short, xi_m_frac, 'o')
plt.xlabel(r'$\theta$ [arcmin]')
plt.ylabel(r'$\xi_-^{athena}/\xi_-^{nicaea} - 1$')
plt.title(r'$\xi_- ath/nic - 1$ vs. $\theta$')
plt.grid(True)
plt.show()

fig.savefig('../Plots/pdf/xi_m_ath_nic_frac_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.pdf')
fig.savefig('../Plots/png/xi_m_ath_nic_frac_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.png')

data = ascii.read('../Sim_Data/xi_p_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.txt', delimiter = ' ')

theta_xi_p_nic = data['col1']
xi_p_nic = data['col2']

f_xi_p_nic = interp1d(theta_xi_p_nic, xi_p_nic)

theta_xi_p_ath_short = []
xi_p_ath_short = []

for i in range(0, len(theta_athena)):
	if theta_athena[i] > min(theta_xi_p_nic):
		theta_xi_p_ath_short.append(theta_athena[i]) 
		xi_p_ath_short.append(xip_athena[i])

fig = plt.figure()

plt.loglog(theta_xi_p_nic, xi_p_nic, 'o', theta_xi_p_ath_short, f_xi_p_nic(theta_xi_p_ath_short), 'ro')
plt.legend(['nic', 'nic_int'], loc='best')
plt.xlabel(r'$\theta$ [arcmin]')
plt.ylabel(r'$\xi_+$')
plt.title(r'nicaea $\xi_+$ vs. $\theta$ interpolated')
plt.xlim( min(theta_xi_p_nic), max(theta_xi_p_nic))
plt.tight_layout()
plt.grid(True)
plt.show()

fig.savefig('../Plots/pdf/intp1d_xi_p_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.pdf')
fig.savefig('../Plots/png/intp1d_xi_p_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.png')

xi_p_nic_interpld = f_xi_p_nic(theta_xi_p_ath_short)
xi_p_frac = []

for i in range(0, len(xi_p_ath_short)):
	xi_p_frac.append((xi_p_ath_short[i]/xi_p_nic_interpld[i]) - 1)

fig = plt.figure()

plt.plot(theta_xi_p_ath_short, xi_p_frac, 'o')
plt.xlabel(r'$\theta$ [arcmin]')
plt.ylabel(r'$\xi_+^{athena}/\xi_+^{nicaea} - 1$')
plt.title(r'$\xi_+ ath/nic - 1$ vs. $\theta$')
plt.grid(True)
plt.show()

fig.savefig('../Plots/pdf/xi_p_ath_nic_frac_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.pdf')
fig.savefig('../Plots/png/xi_p_ath_nic_frac_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.png')

fig = plt.figure()

plt.loglog(theta_xi_p_ath_short, f_xi_p_nic(theta_xi_p_ath_short), 'ro', theta_xi_p_ath_short, xi_p_ath_short, 'o')
plt.tight_layout()
plt.grid(True)
plt.show()

