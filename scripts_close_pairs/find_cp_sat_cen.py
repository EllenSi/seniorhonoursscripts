from scipy import spatial
import numpy as np
from astropy.io import fits, ascii
import matplotlib.pyplot as plt

import sys
sys.setrecursionlimit(1000000)


#galtype = sys.argv[1]
arcs = float(sys.argv[2])
#lines of sight = sys.argv[3]

print(arcs)

r = arcs/60.

with open("../Sim_Data/sat_cen/"+sys.argv[1]+"_"+sys.argv[3]+".asc") as f:
    data = f.read()

data = data.split('\n')
data2 = data[:-1]
data_list = []
example_list = []

print(len(data))

for i in data2:
    data_list.append(i.split(','))

for j in range(0, len(data_list)):
    for k in data_list[j]:
        example_list.append([float(l) for l in k .split(' ')])

x_list = []
y_list = []

for i in range(0,len(example_list)):
    for j in range(0,len(example_list[i])):
        if j == 0:
            x_list.append(example_list[i][j])
        elif j == 1:
            y_list.append(example_list[i][j])

tree = spatial.KDTree(zip(np.array(x_list).ravel(), np.array(y_list).ravel()))
print(tree)

f = open('../Sim_Data/close_pairs/close_pairs_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.txt','w')

close_pairs = list(tree.query_pairs(r))

for i in close_pairs:
    f.write(str(i) + '\n')
    print(i)
   
f.close()

