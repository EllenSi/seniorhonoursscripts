from astropy.table import Table
import fitsio
from fitsio import FITS,FITSHDR
import numpy as np
from astropy.io import fits, ascii
from astropy.table import table
import sys

# galtype = sys.argv[1]
# lines of sight = sys.argv[2]

with open('../Sim_Data/close_pairs/close_pairs_'+sys.argv[1]+'_5_'+sys.argv[2]+'.txt') as f:
    data = f.read()

data = data.split('\n')

f.close()

gal_array = []

for row in data[:-1]:
    gal_one, gal_two = (float(x.strip("(),")) for x in row.split())
    gal_array.append(gal_one)
    gal_array.append(gal_two)  

gal_array.sort()
gal_array = list(set(gal_array))
gal_array.sort()

with open('../Sim_Data/cen_sat/'+sys.argv[1]+'_'+sys.argv[2]+'.asc') as f:
    data = f.read()

data = data.split('\n')
data_new = data[:-1]

data_list = []
properties_list = []

for i in data_new:
    data_list.append(i.split(' '))

x = []
y = []
s1 = []
s2 = []
z_s = []
z_p = []

for j in range(0, len(data_list)):
    properties_list.append([float(l) for l in data_list[j]])

for i in range(0,len(properties_list)):
    
            x.append(properties_list[i][0])     
            y.append(properties_list[i][1])
            s1.append(properties_list[i][3])
            s2.append(properties_list[i][4])
            z_p.append(properties_list[i][5])
            z_s.append(properties_list[i][6])


print(len(gal_array)) 

x = np.array(x)
y = np.array(y)
s1 = np.array(s1)
s2 = np.array(s2)
z_p = np.array(z_p)
z_s = np.array(z_s)

mask = np.ones(len(x), dtype=bool)
mask[gal_array] = False
x_red = x[mask,...]

mask = np.ones(len(y), dtype=bool)
mask[gal_array] = False
y_red = y[mask,...] 

mask = np.ones(len(s1), dtype=bool)
mask[gal_array] = False
s1_red = s1[mask,...]

mask = np.ones(len(s2), dtype=bool)
mask[gal_array] = False
s2_red = s2[mask,...]
 
mask = np.ones(len(z_p), dtype=bool)
mask[gal_array] = False
z_p_red = z_p[mask,...]

mask = np.ones(len(z_s), dtype=bool)
mask[gal_array] = False
z_s_red = z_s[mask,...]
         
ascii.write([x_red, y_red,s1_red,s2_red, z_s_red,z_p_red], '../Sim_Data/reduced/'+sys.argv[1]+'_reduced_both_'+sys.argv[2]+'.asc', names=['x','y','s1', 's2','z_s','z_p'])

new_col1 = fits.ColDefs([fits.Column(name='x_arcmin', format='D',array=np.array(x_red))])
new_col2 = fits.ColDefs([fits.Column(name='y_arcmin', format='D',array=np.array(y_red))])
new_col3 = fits.ColDefs([fits.Column(name='shear1', format='D',array=np.array(s1_red))])
new_col4 = fits.ColDefs([fits.Column(name='shear2', format='D',array=np.array(s2_red))])
hdu = fits.BinTableHDU.from_columns(new_col1 + new_col2 + new_col3 + new_col4)
hdu.writeto('../Sim_Data/reduced/'+sys.argv[1]+'_reduced_both_'+sys.argv[2]+'.fits')


        
