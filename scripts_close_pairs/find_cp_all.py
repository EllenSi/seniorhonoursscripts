from scipy import spatial
from astropy.io import fits, ascii

import matplotlib.pyplot as plt
import sys
import numpy as np

sys.setrecursionlimit(1000000)

arcs = float(sys.argv[1])
# lines of sight = sys.argv[2]

hdulist = fits.open('../Sim_Data/catalogs/FullGalCatalogSDSS_zmax_0.3new_LOS'+sys.argv[2]+'.fits')
tbdata = hdulist[1].data

x_array = tbdata.field(1)
y_array = tbdata.field(2)

print(len(x_array))

tree = spatial.KDTree(zip(x_array, y_array))
print(tree)

r = arcs/60.
print(r)

f = open('../Sim_Data/close_pairs/close_pairs_all_'+ sys.argv[1]+'_'+sys.argv[2]+'.txt', 'w')

close_pairs = list(tree.query_pairs(r))

for i in close_pairs:
    f.write(str(i) + '\n')
    print(i)
    
f.close()
