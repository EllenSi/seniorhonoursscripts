import sys

# galtype = sys.argv[1]
# cuts = sys.argv[2]
# lines of sight = argv[3]


# opens and reads file with test data
with open('../Sim_Data/nicaea_data/xi_m_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'_5') as f:
    data = f.read()

# splits data into seperate rows
data = data.split('\n')

index = 0

for i in range(0, len(data)):
	if data[i] == '# th[\']             00':
		index = i + 1

xi_m_theory = open('../Sim_Data/nicaea_data/xi_m_nic_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'_5.txt', 'w')

for i in range(index, len(data)):
	if i < len(data) - 1:
		xi_m_theory.write(data[i].lstrip('  ') + '\n')
	else:
		xi_m_theory.write(data[i].lstrip('  '))

xi_m_theory.close()
f.close()

# opens and reads file with test data
with open('../Sim_Data/nicaea_data/xi_p_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'_5') as f:
    data = f.read()

# splits data into seperate rows
data = data.split('\n')

index = 0

for i in range(0, len(data)):
	if data[i] == '# th[\']             00':
		index = i + 1

xi_p_theory = open('../Sim_Data/nicaea_data/xi_p_nic_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'_5.txt','w')

for i in range(index, len(data)):
	if i < len(data) - 1:
		xi_p_theory.write(data[i].lstrip('  ') + '\n')
	else:
		xi_p_theory.write(data[i].lstrip('  '))

xi_p_theory.close()
f.close()
