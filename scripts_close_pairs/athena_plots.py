from astropy.io import fits
import matplotlib.pyplot as plt
import sys

#galaxy type = sys.argv[1]
#cuts = sys.argv[2]
#lines of sight = sys.argv[3]

hdulist = fits.open('../Sim_Data/athena_data/xi_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.fits')
tbdata = hdulist[1].data

theta = tbdata.field(0)
xip = tbdata.field(1)
xim = tbdata.field(2)

fig = plt.figure()
plt.loglog(theta, xip, 'bo')
plt.xlabel(r'$\theta$ [arcmin]')
plt.ylabel(r'$\xi_+$')
plt.title(r'$\xi_+$ vs. $\theta$ - '+ sys.argv[1]+' - '+sys.argv[2])
plt.xlim( min(theta), max(theta))
plt.tight_layout()
plt.grid(True)
plt.show()

fig.savefig('../Plots/pdf/ath/xip_theta_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.pdf')
fig.savefig('../Plots/png/ath/xip_theta_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.png')

fig = plt.figure()
plt.loglog(theta, xim, 'bo')
plt.xlabel(r'$\theta$ [arcmin]')
plt.ylabel(r'$\xi_-$')
plt.title(r'$\xi_-$ vs. $\theta$ - '+ sys.argv[1]+' - '+sys.argv[2])
plt.xlim(min(theta), max(theta))
plt.tight_layout()
plt.grid(True)
plt.show()

fig.savefig('../Plots/pdf/ath/xim_theta_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.pdf')
fig.savefig('../Plots/png/ath/xim_theta_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.png')
