import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.io import ascii
from matplotlib.ticker import Locator
from minor_ticks import *
from scipy.interpolate import interp1d

import sys

# galtype = sys.argv[1]

th = []
xim_none = []
xip_none = []
xim_both = []
xip_both = []
xim_faint = []
xip_faint = []
xim_random = []
xip_random = []
xim_random_5 = []
xip_random_5 = []

for i in range(74,100):
    data1 = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_'+sys.argv[1]+'_none_'+str(i)+'.txt', delimiter=' ' )
    data2 = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_'+sys.argv[1]+'_none_'+str(i)+'.txt', delimiter=' ' )
    data3 = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_'+sys.argv[1]+'_both_'+str(i)+'.txt', delimiter=' ' )
    data4 = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_'+sys.argv[1]+'_both_'+str(i)+'.txt', delimiter=' ' )
    data5 = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_'+sys.argv[1]+'_faint_'+str(i)+'.txt', delimiter=' ' )
    data6 = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_'+sys.argv[1]+'_faint_'+str(i)+'.txt', delimiter=' ' )
    data7 = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_'+sys.argv[1]+'_random_'+str(i)+'_5.txt', delimiter=' ' )
    data8 = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_'+sys.argv[1]+'_random_'+str(i)+'_5.txt', delimiter=' ' )
    data9 = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_'+sys.argv[1]+'_random_'+str(i)+'.txt', delimiter=' ' )
    data10 = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_'+sys.argv[1]+'_random_'+str(i)+'.txt', delimiter=' ' )


    
    theta1 = data1['col1']
    xi_m_none = data1['col2']
    xi_p_none = data2['col2']
    xi_m_both = data3['col2']
    xi_p_both = data4['col2']
    xi_m_faint = data5['col2']
    xi_p_faint = data6['col2']
    xi_m_random_5 = data7['col2']
    xi_p_random_5 = data8['col2']
    xi_m_random = data9['col2']
    xi_p_random = data10['col2']

    th.append(theta1)
    xim_none.append(xi_m_none)
    xip_none.append(xi_p_none)
    xim_both.append(xi_m_both)
    xip_both.append(xi_p_both)
    xim_faint.append(xi_m_faint)
    xip_faint.append(xi_p_faint)
    xim_random.append(xi_m_random)
    xip_random.append(xi_p_random)
    xim_random_5.append(xi_m_random_5)
    xip_random_5.append(xi_p_random_5)
    
hdulist = fits.open('../Sim_Data/athena_data/xi_all_random_74_5.fits')

tbdata = hdulist[1].data

theta_array = tbdata.field(0)
    
th_ath_short = []

for i in range(0, len(theta_array)):
    if theta_array[i] >= min(theta1) and  theta_array[i] <= max(theta1):
        th_ath_short.append(theta_array[i]) 

xim_nic_ath_none = []
xip_nic_ath_none = []
xim_nic_ath_both = []
xip_nic_ath_both = []
xim_nic_ath_faint = []
xip_nic_ath_faint = []
xim_nic_ath_random = []
xip_nic_ath_random = []
xim_nic_ath_random_5 = []
xip_nic_ath_random_5 = []

for i in range(0,len(th)):

    f_xi_m_nic_none = interp1d(th[i], xim_none[i])
    xim_nic_ath_none.append(f_xi_m_nic_none(th_ath_short))
    f_xi_p_nic_none = interp1d(th[i], xip_none[i])
    xip_nic_ath_none.append(f_xi_p_nic_none(th_ath_short))

    f_xi_m_nic_both = interp1d(th[i], xim_both[i])
    xim_nic_ath_both.append(f_xi_m_nic_both(th_ath_short))
    f_xi_p_nic_both = interp1d(th[i], xip_both[i])
    xip_nic_ath_both.append(f_xi_p_nic_both(th_ath_short))

    f_xi_m_nic_faint = interp1d(th[i], xim_faint[i])
    xim_nic_ath_faint.append(f_xi_m_nic_faint(th_ath_short))
    f_xi_p_nic_faint = interp1d(th[i], xip_faint[i])
    xip_nic_ath_faint.append(f_xi_p_nic_faint(th_ath_short))

    f_xi_m_nic_random = interp1d(th[i], xim_random[i])
    xim_nic_ath_random.append(f_xi_m_nic_random(th_ath_short))
    f_xi_p_nic_random = interp1d(th[i], xip_random[i])
    xip_nic_ath_random.append(f_xi_p_nic_random(th_ath_short))

    f_xi_m_nic_random_5 = interp1d(th[i], xim_random_5[i])
    xim_nic_ath_random_5.append(f_xi_m_nic_random_5(th_ath_short))
    f_xi_p_nic_random_5 = interp1d(th[i], xip_random_5[i])
    xip_nic_ath_random_5.append(f_xi_p_nic_random_5(th_ath_short)) 

dct_th = {}

dct_nic_xi_p_none = {}
dct_nic_xi_m_none = {}
dct_nic_xi_p_both = {}
dct_nic_xi_m_both = {}
dct_nic_xi_p_faint = {}
dct_nic_xi_m_faint = {}
dct_nic_xi_p_random = {}
dct_nic_xi_m_random = {}
dct_nic_xi_p_random_5 = {}
dct_nic_xi_m_random_5 = {}

for i in range(0,len(th_ath_short)):
    dct_th[i] = []
    dct_nic_xi_p_none[i] = []
    dct_nic_xi_m_none[i] = []
    dct_nic_xi_p_both[i] = []
    dct_nic_xi_m_both[i] = []
    dct_nic_xi_p_faint[i] = []
    dct_nic_xi_m_faint[i] = []
    dct_nic_xi_p_random[i] = []
    dct_nic_xi_m_random[i] = []
    dct_nic_xi_p_random_5[i] = []
    dct_nic_xi_m_random_5[i] = []
    
for i in range(0,len(xip_nic_ath_none)):
    for j in range(0,len(th_ath_short)):
        dct_th[j].append(th_ath_short[j])
        dct_nic_xi_p_none[j].append(xip_nic_ath_none[i][j])
        dct_nic_xi_m_none[j].append(xim_nic_ath_none[i][j])
        dct_nic_xi_p_both[j].append(xip_nic_ath_both[i][j])
        dct_nic_xi_m_both[j].append(xim_nic_ath_both[i][j])
        dct_nic_xi_p_faint[j].append(xip_nic_ath_faint[i][j])
        dct_nic_xi_m_faint[j].append(xim_nic_ath_faint[i][j])
        dct_nic_xi_p_random[j].append(xip_nic_ath_random[i][j])
        dct_nic_xi_m_random[j].append(xim_nic_ath_random[i][j])
        dct_nic_xi_p_random_5[j].append(xip_nic_ath_random_5[i][j])
        dct_nic_xi_m_random_5[j].append(xim_nic_ath_random_5[i][j])

dct_xip_none = {}
dct_xim_none = {}
dct_xip_both = {}
dct_xim_both = {}
dct_xip_faint = {}
dct_xim_faint = {}
dct_xip_random = {}
dct_xim_random = {}
dct_xip_random_5 = {}
dct_xim_random_5 = {}

for i in range(0,15):
    dct_xip_none[i] = []
    dct_xim_none[i] = []
    dct_xip_both[i] = []
    dct_xim_both[i] = []
    dct_xip_faint[i]  = []
    dct_xim_faint[i] = []
    dct_xip_random[i] = []
    dct_xim_random[i] = []
    dct_xip_random_5[i] = []
    dct_xim_random_5[i] = []

for i in range(74,100):

    hdulist = fits.open('../Sim_Data/athena_data/xi_'+sys.argv[1]+'_none_'+str(i)+'.fits')
    tbdata = hdulist[1].data

    xip_none = tbdata.field(1)
    xim_none = tbdata.field(2)

    hdulist = fits.open('../Sim_Data/athena_data/xi_'+sys.argv[1]+'_both_'+str(i)+'.fits')
    tbdata = hdulist[1].data

    xip_both = tbdata.field(1)
    xim_both = tbdata.field(2)

    hdulist = fits.open('../Sim_Data/athena_data/xi_'+sys.argv[1]+'_faint_'+str(i)+'.fits')
    tbdata = hdulist[1].data

    xip_faint = tbdata.field(1)
    xim_faint = tbdata.field(2)

    hdulist = fits.open('../Sim_Data/athena_data/xi_'+sys.argv[1]+'_random_'+str(i)+'.fits')
    tbdata = hdulist[1].data

    xip_random = tbdata.field(1)
    xim_random = tbdata.field(2)

    hdulist = fits.open('../Sim_Data/athena_data/xi_'+sys.argv[1]+'_random_'+str(i)+'_5.fits')
    tbdata = hdulist[1].data

    xip_random_5 = tbdata.field(1)
    xim_random_5 = tbdata.field(2)

    for j in th_ath_short:
        
        dct_xip_none[th_ath_short.index(j)].append(xip_none[th_ath_short.index(j)])
        dct_xim_none[th_ath_short.index(j)].append(xim_none[th_ath_short.index(j)])

    for j in th_ath_short:
        
        dct_xip_both[th_ath_short.index(j)].append(xip_both[th_ath_short.index(j)])
        dct_xim_both[th_ath_short.index(j)].append(xim_both[th_ath_short.index(j)])
        
    for j in th_ath_short:
        dct_xip_faint[th_ath_short.index(j)].append(xip_faint[th_ath_short.index(j)])
        dct_xim_faint[th_ath_short.index(j)].append(xim_faint[th_ath_short.index(j)])

    for j in th_ath_short:
        
        dct_xip_random[th_ath_short.index(j)].append(xip_random[th_ath_short.index(j)])
        dct_xim_random[th_ath_short.index(j)].append(xim_random[th_ath_short.index(j)])

    for j in th_ath_short:
        
        dct_xip_random_5[th_ath_short.index(j)].append(xip_random_5[th_ath_short.index(j)])
        dct_xim_random_5[th_ath_short.index(j)].append(xim_random_5[th_ath_short.index(j)])

frac_xip_both = {}
frac_xip_faint  = {}
frac_xip_random = {}
frac_xip_random_5 = {}

frac_xim_both = {}
frac_xim_faint  = {}
frac_xim_random = {}
frac_xim_random_5 = {}


theta = []

for i in range(0,len(dct_th)):

    frac_xip_both[i] = []
    frac_xip_faint[i] = []
    frac_xip_random[i] = []
    frac_xip_random_5[i] = []
    
    frac_xim_both[i] = []
    frac_xim_faint[i] = []
    frac_xim_random[i] = []
    frac_xim_random_5[i] = []

    rng = len(dct_th[i])

    theta.append(dct_th[i][0])
    
    for j in range(0,rng):
        
        fr_xip_both = (dct_xip_both[i][j]/dct_xip_none[i][j])*(dct_nic_xi_p_none[i][j]/dct_nic_xi_p_both[i][j]) - 1.
        fr_xim_both = (dct_xim_both[i][j]/dct_xim_none[i][j])*(dct_nic_xi_m_none[i][j]/dct_nic_xi_m_both[i][j]) - 1.
        fr_xip_faint = (dct_xip_faint[i][j]/dct_xip_none[i][j])*(dct_nic_xi_p_none[i][j]/dct_nic_xi_p_faint[i][j]) - 1.
        fr_xim_faint = (dct_xim_faint[i][j]/dct_xim_none[i][j])*(dct_nic_xi_m_none[i][j]/dct_nic_xi_m_faint[i][j]) - 1.
        fr_xip_random = (dct_xip_random[i][j]/dct_xip_none[i][j])*(dct_nic_xi_p_none[i][j]/dct_nic_xi_p_random[i][j]) - 1.
        fr_xim_random = (dct_xim_random[i][j]/dct_xim_none[i][j])*(dct_nic_xi_m_none[i][j]/dct_nic_xi_m_random[i][j]) - 1.
        fr_xip_random_5 = (dct_xip_random_5[i][j]/dct_xip_none[i][j])*(dct_nic_xi_p_none[i][j]/dct_nic_xi_p_random_5[i][j]) - 1.
        fr_xim_random_5 = (dct_xim_random_5[i][j]/dct_xim_none[i][j])*(dct_nic_xi_m_none[i][j]/dct_nic_xi_m_random_5[i][j]) - 1.

        frac_xip_both[i].append(fr_xip_both)
        frac_xip_faint[i].append(fr_xip_faint)
        frac_xip_random[i].append(fr_xip_random)
        frac_xip_random_5[i].append(fr_xip_random_5)


        frac_xim_both[i].append(fr_xim_both)
        frac_xim_faint[i].append(fr_xim_faint)
        frac_xim_random[i].append(fr_xim_random)
        frac_xim_random_5[i].append(fr_xim_random_5)

mean_fr_xip_both = []
mean_fr_xim_both = []
mean_fr_xip_faint = []
mean_fr_xim_faint = []
mean_fr_xip_random = []
mean_fr_xim_random = []
mean_fr_xip_random_5 = []
mean_fr_xim_random_5 = []

sig_fr_xip_both = []
sig_fr_xim_both = []
sig_fr_xip_faint = []
sig_fr_xim_faint = []
sig_fr_xip_random = []
sig_fr_xim_random = []
sig_fr_xip_random_5 = []
sig_fr_xim_random_5 = []

for i in range(0,len(dct_th)):

    mean_fr_xip_both.append(np.mean(np.array(frac_xip_both[i])))
    mean_fr_xim_both.append(np.mean(np.array(frac_xim_both[i])))    
    sig_fr_xip_both.append(np.std(np.array(frac_xip_both[i]))/np.sqrt(len(frac_xip_both[i])))
    sig_fr_xim_both.append(np.std(np.array(frac_xim_both[i]))/np.sqrt(len(frac_xim_both[i])))

    mean_fr_xip_faint.append(np.mean(np.array(frac_xip_faint[i])))
    mean_fr_xim_faint.append(np.mean(np.array(frac_xim_faint[i])))    
    sig_fr_xip_faint.append(np.std(np.array(frac_xip_faint[i]))/np.sqrt(len(frac_xip_faint[i])))
    sig_fr_xim_faint.append(np.std(np.array(frac_xim_faint[i]))/np.sqrt(len(frac_xim_faint[i])))

    mean_fr_xip_random.append(np.mean(np.array(frac_xip_random[i])))
    mean_fr_xim_random.append(np.mean(np.array(frac_xim_random[i])))    
    sig_fr_xip_random.append(np.std(np.array(frac_xip_random[i]))/np.sqrt(len(frac_xip_random[i])))
    sig_fr_xim_random.append(np.std(np.array(frac_xim_random[i]))/np.sqrt(len(frac_xim_random[i])))

    mean_fr_xip_random_5.append(np.mean(np.array(frac_xip_random_5[i])))
    mean_fr_xim_random_5.append(np.mean(np.array(frac_xim_random_5[i])))    
    sig_fr_xip_random_5.append(np.std(np.array(frac_xip_random_5[i]))/np.sqrt(len(frac_xip_random_5[i])))
    sig_fr_xim_random_5.append(np.std(np.array(frac_xim_random_5[i]))/np.sqrt(len(frac_xim_random_5[i])))


fig = plt.figure()
plt.errorbar(th_ath_short,mean_fr_xip_both, yerr=sig_fr_xip_both, fmt='ro',label='both removed - 5" ')
plt.errorbar(th_ath_short,mean_fr_xip_faint, yerr=sig_fr_xip_faint, fmt='go',label='faint removed - 15" ')
plt.errorbar(th_ath_short,mean_fr_xip_random, yerr=sig_fr_xip_random, fmt='bo',label='random removed - 15" ')
plt.errorbar(th_ath_short,mean_fr_xip_random_5, yerr=sig_fr_xip_random_5, fmt='mo',label='random removed - 5" ')
plt.xscale('log')
plt.xlabel(r'$\theta$ (arcmin)',fontsize=20)
plt.xlim([1,max(th_ath_short)+20])
plt.ylim([-0.25,0.05])
plt.ylabel(r'$\beta^{cuts}_{+}$',fontsize=25)
#plt.title(r'Ratio of $\xi_{+}$ with cuts and no cuts')
plt.legend(loc='lower right', shadow=True)
plt.tight_layout()
plt.show()
fig.savefig('../Plots/png/frac_xip_'+sys.argv[1]+'_cuts.png')

fig = plt.figure()
plt.errorbar(th_ath_short,mean_fr_xim_both, yerr=sig_fr_xim_both, fmt='ro',label='both removed - 5" ')
plt.errorbar(th_ath_short,mean_fr_xim_faint, yerr=sig_fr_xim_faint, fmt='go',label='faint removed - 15" ')
plt.errorbar(th_ath_short,mean_fr_xim_random, yerr=sig_fr_xim_random, fmt='bo',label='random removed - 15" ')
plt.errorbar(th_ath_short,mean_fr_xim_random_5, yerr=sig_fr_xim_random_5, fmt='mo',label='random removed - 5" ')
plt.xscale('log')
plt.xlim([1,max(th_ath_short)+20])
plt.ylim([-0.6,0])
plt.xlabel(r'$\theta$ (arcmin)',fontsize=20)
plt.ylabel(r'$\beta^{cuts}_{-}$',fontsize=25)
#plt.title(r'Ratio of $\xi_{-}$ with cuts and no cuts')
plt.legend(loc='best', shadow=True)
plt.tight_layout()
plt.show()
fig.savefig('../Plots/png/frac_xim_'+sys.argv[1]+'_cuts.png')


