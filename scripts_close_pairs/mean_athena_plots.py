import numpy as np
from astropy.io import fits, ascii
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
from astropy.table import Table

# galaxy type = sys.argv[1]
# cuts = sys.argv[2]

dct_t = {}
dct_xip = {}
dct_xim = {}

for j in range(0,15):
    dct_t[j] = []
    dct_xip[j] =  []
    dct_xim[j] = []

for i in range(74,100):
     
    hdulist = fits.open('../Sim_Data/athena_data/xi_'+sys.argv[1]+'_'+sys.argv[2]+'_'+str(i)+'.fits')
    tbdata = hdulist[1].data

    th = tbdata.field(0)
    xip = tbdata.field(1)
    xim = tbdata.field(2)

    for j in range(0,len(th)):
        
        dct_t[j].append(tbdata.field(0)[j])
        dct_xip[j].append(tbdata.field(1)[j])
        dct_xim[j].append(tbdata.field(2)[j])
        
mean_xip = []
mean_xim = []        
stdev_xip = []
stdev_xim = []

for k in range(0,len(dct_t)):
    mean_xip.append(np.mean(np.array(dct_xip[k])))
    mean_xim.append(np.mean(np.array(dct_xim[k])))    
    stdev_xip.append(np.std(np.array(dct_xip[k])))
    stdev_xim.append(np.std(np.array(dct_xim[k])))

err_mean_xip = []
err_mean_xim = []

for i in range(0,len(stdev_xim)):

    err_mean_xip.append(stdev_xip[i]/np.sqrt(len(stdev_xip)))
    err_mean_xim.append(stdev_xim[i]/np.sqrt(len(stdev_xim)))

th2 = []
xim = []
xip = []

for i in range(74,100):
    data1 = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_'+sys.argv[1]+'_'+sys.argv[2]+'_'+str(i)+'_ext.txt', delimiter=' ' )
    data2 = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_'+sys.argv[1]+'_'+sys.argv[2]+'_'+str(i)+'_ext.txt', delimiter=' ' )

    theta1 = data1['col1']
    xi_m = data1['col2']

    theta2 = data2['col1']
    xi_p = data2['col2']

    th2.append(theta1)
    xim.append(xi_m)
    xip.append(xi_p)

fig = plt.figure()
for i in range(0,len(th2)):
    if i == len(th2) -1:
        plt.loglog(th2[i], xip[i], '-',color='0.75', lw=0.25, label='theory predictions')
    else:
        plt.loglog(th2[i], xip[i], '-',color='0.75', lw=0.25)
        
ax = fig.add_subplot(111)
ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')
#plt.errorbar(th, mean_xip, yerr=err_mean_xip, fmt='bo')
plt.errorbar(th,mean_xip, yerr=stdev_xip,fmt='ro',label='measurements')
#plt.title(r'Mean $\xi_{+}$ of Measurements and $\xi_{+}$ of of Predicitons vs. $\theta$ - '+ sys.argv[1]+' - '+sys.argv[2])
plt.xlabel(r'$\theta$ [arcmin]', fontsize=20)
plt.ylabel(r'$\xi_{+}^{none}$',fontsize=25)
plt.xlim([min(theta1),max(theta1)])
plt.legend(loc='best')
plt.tight_layout()
plt.show()
fig.savefig('../Plots/pdf/ath+nic_xip_'+sys.argv[1]+'_'+sys.argv[2]+'_ext.pdf')
fig.savefig('../Plots/png/ath+nic_xip_'+sys.argv[1]+'_'+sys.argv[2]+'_ext.png')

fig = plt.figure()
for i in range(0,len(th)):

    if i == len(th) -1:
        plt.loglog(th2[i], xim[i], '-',color='0.75', lw=0.25,label='theory predictions')
    else:
        plt.loglog(th2[i],xim[i],'-',color='0.75',lw=0.25)
ax = fig.add_subplot(111)
ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')
#plt.errorbar(th, mean_xim, yerr=err_mean_xim, fmt='bo')
plt.errorbar(th,mean_xim, yerr=stdev_xim,fmt='ro',label='measurements')
#plt.title(r'Mean $\xi_{-}$ of Measurements and $\xi_{-}$ of of Predicitons vs. $\theta$ - '+ sys.argv[1]+' - '+sys.argv[2])
plt.xlabel(r'$\theta$ [arcmin]', fontsize=20)
plt.ylabel(r'$\xi_{-}^{none}$', fontsize=25)
plt.xlim([min(theta2),max(theta2)])
#plt.ylim([1e-25,1e-5])
plt.legend(loc='best')
plt.tight_layout()
plt.show()
fig.savefig('../Plots/pdf/ath+nic_xim_'+sys.argv[1]+'_'+sys.argv[2]+'_ext.pdf')
fig.savefig('../Plots/png/ath+nic_xim_'+sys.argv[1]+'_'+sys.argv[2]+'_ext.png')




            
            
            

    
    


