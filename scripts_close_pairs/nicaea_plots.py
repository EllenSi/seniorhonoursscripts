import numpy as np
from astropy.io import fits, ascii
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
from astropy.table import Table
from scipy.interpolate import interp1d

# galtype = sys.argv[1]
# cuts = sys.argv[2]
# lines of sight = sys.argv[3]

th = []
xim = []
xip = []

for i in range(74,100):
    data1 = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_'+sys.argv[1]+'_'+sys.argv[2]+'_'+str(i)+'.txt', delimiter=' ' )
    data2 = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_'+sys.argv[1]+'_'+sys.argv[2]+'_'+str(i)+'.txt', delimiter=' ' )

    theta1 = data1['col1']
    xi_m = data1['col2']

    theta2 = data2['col1']
    xi_p = data2['col2']

    th.append(theta1)
    xim.append(xi_m)
    xip.append(xi_p)

hdulist = fits.open('../Sim_Data/athena_data/xi_'+sys.argv[1]+'_'+sys.argv[2]+'_74.fits')

tbdata = hdulist[1].data

theta_array = tbdata.field(0)
xip_array = tbdata.field(1)
xim_array = tbdata.field(2)

th_ath_short = []

for i in range(0, len(theta_array)):
    if theta_array[i] >= min(theta1) and  theta_array[i] <= max(theta1):
        th_ath_short.append(theta_array[i]) 

xim_nic_ath = []
xip_nic_ath = []

for i in range(0,len(th)):

    f_xi_m_nic = interp1d(th[i], xim[i])
    xim_nic_ath.append(f_xi_m_nic(th_ath_short))
    f_xi_p_nic = interp1d(th[i], xip[i])
    xip_nic_ath.append(f_xi_p_nic(th_ath_short))

dct_th = {}
dct_xi_p = {}
dct_xi_m = {}

for i in range(0,len(th_ath_short)):
    dct_xi_p[i] =  []
    dct_xi_m[i] = []
    
for i in range(0,len(xip_nic_ath)):
    for j in range(0,len(th_ath_short)):
        dct_xi_p[j].append(xip_nic_ath[i][j])
        dct_xi_m[j].append(xim_nic_ath[i][j])

mean_xip_nic = []
mean_xim_nic = []        
stdev_xip_nic = []
stdev_xim_nic = []

for k in range(0,len(dct_xi_p)):
    mean_xip_nic.append(np.mean(np.array(dct_xi_p[k])))
    mean_xim_nic.append(np.mean(np.array(dct_xi_m[k])))    
    stdev_xip_nic.append(np.std(np.array(dct_xi_p[k])))
    stdev_xim_nic.append(np.std(np.array(dct_xi_m[k])))

err_mean_xip_nic = []
err_mean_xim_nic = []

for i in range(0,len(stdev_xim_nic)):

    err_mean_xip_nic.append(stdev_xip_nic[i]/np.sqrt(len(stdev_xip_nic)))
    err_mean_xim_nic.append(stdev_xim_nic[i]/np.sqrt(len(stdev_xim_nic)))

print ' making plot...'
 
fig = plt.figure()

for i in range(0,len(th)):
    
    plt.loglog(th[i], xip[i], '-',color='0.75', lw=0.25)

ax = fig.add_subplot(111)

ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')

plt.errorbar(th_ath_short, mean_xip_nic, yerr=err_mean_xip_nic, fmt='bo',ms=4)
plt.errorbar(th_ath_short, mean_xip_nic, yerr=stdev_xip_nic,fmt='ro',ms=4)
plt.xlim([min(theta1),max(theta1)])
plt.ylim([5e-8,3e-5])
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\xi^{+}_{nicaea}$')

plt.title(sys.argv[1]+' galaxies with '+sys.argv[2]+' cuts, lines of sight 74-99')

plt.tight_layout()  
#plt.show()

fig.savefig('../Plots/png/nic/variation_xip_'+sys.argv[1]+'_'+sys.argv[2]+'.png')

print ' making plot...'
 
fig = plt.figure()

for i in range(0,len(th)):
    
    plt.loglog(th[i], xim[i], '-',color='0.75', lw=0.25)

ax = fig.add_subplot(111)

ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')

plt.errorbar(th_ath_short, mean_xim_nic, yerr=err_mean_xim_nic, fmt='bo',ms=4)
plt.errorbar(th_ath_short,mean_xim_nic, yerr=stdev_xim_nic, fmt='ro',ms=4)
plt.xlim([min(theta1),max(theta1)])
plt.ylim([5e-8,2e-6])

plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\xi^{-}_{nicaea}$')

plt.title(sys.argv[1]+' galaxies with '+sys.argv[2]+' cuts, lines of sight 74-99')

plt.tight_layout()  
#plt.show()

fig.savefig('../Plots/png/nic/variation_xim_'+sys.argv[1]+'_'+sys.argv[2]+'.png')

data = Table([th_ath_short, mean_xip_nic, mean_xim_nic, err_mean_xip_nic, err_mean_xim_nic,stdev_xip_nic,stdev_xim_nic], names=[ 'th_nic', 'mean_xip_nic','mean_xim_nic','mean_err_xip_nic','mean_err_xim_nic','stdev_xip_nic','stdev_xim_nic'])
ascii.write(data, '../Sim_Data/stdev/mean_nic_'+sys.argv[1]+'_'+sys.argv[2]+'.txt')
