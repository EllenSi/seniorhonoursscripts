import numpy as np
from astropy.io import fits, ascii
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
from astropy.table import Table

# galaxy type = sys.argv[1]
# cuts = sys.argv[2]

data = ascii.read('../Sim_Data/stdev/mean_ath_'+sys.argv[1]+'_'+sys.argv[2]+'.txt')

th1 = list(data['th'])
mn_err_xip = data['mean_err_xip']
mr_err_xim = data['mean_err_xim']
std_xip = data['stdev_xip']
std_xim = data['stdev_xim']

data = ascii.read('../Sim_Data/stdev/mean_nic_'+sys.argv[1]+'_'+sys.argv[2]+'.txt')

th2 = list(data['th_nic'])
mn_err_xip_nic = data['mean_err_xip_nic']
mr_err_xim_nic = data['mean_err_xim_nic']
std_xip_nic = data['stdev_xip_nic']
std_xim_nic = data['stdev_xim_nic']

frac_std_xip = []
frac_std_xim = []

for i in th2:
    frac_std_xip.append(std_xip_nic[th2.index(i)]/std_xip[th1.index(i)])
    frac_std_xim.append(std_xim_nic[th2.index(i)]/std_xim[th1.index(i)])

fig = plt.figure()
plt.loglog(th2, frac_std_xip, 'ro')
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\frac{\sigma_{nicaea}}{\sigma_{athena}}$ $\xi^{+}$',fontsize=20)
plt.title(r'Ratio of std dev. of nicaea and athena data for $\xi^{+}$ vs. $\theta$ - '+sys.argv[1]+'  - '+sys.argv[2])
plt.tight_layout()
plt.show()
fig.savefig('../Plots/png/errors/ratio_err_na_xip_'+sys.argv[1]+'_'+sys.argv[2]+'.png')


fig = plt.figure()
plt.loglog(th2, frac_std_xim,'ro')
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\frac{\sigma_{nicaea}}{\sigma_{athena}}$ $\xi^{-}$',fontsize=20)
plt.title(r'Ratio of std dev. of nicaea and athena data for $\xi^{-}$ vs. $\theta$ - '+sys.argv[1]+'  - '+sys.argv[2])
plt.tight_layout()
plt.show()

fig.savefig('../Plots/png/errors/ratio_err_na_xim_'+sys.argv[1]+'_'+sys.argv[2]+'.png')


