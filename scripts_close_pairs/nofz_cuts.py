from astropy.io import fits, ascii
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys

# cuts = sys.argv[1]
# lines of sight = sys.argv[2]

with open('../Sim_Data/reduced/all_reduced_'+sys.argv[1]+'_'+sys.argv[2]+'_5.asc') as f:
    data = f.read()

data = data.split('\n')
data_new = data[1:-1]

data_list = []
properties_list = []

print(len(data_new))

for i in data_new:
    data_list.append(i.split(' '))

x = []
y = []
s1 = []
s2 = []
z_p = []
z_s = []

for j in range(0, len(data_list)):
    properties_list.append([float(l) for l in data_list[j]])

for i in range(0,len(properties_list)):
    
            x.append(properties_list[i][0])     
            y.append(properties_list[i][1])
            s1.append(properties_list[i][2])
            s2.append(properties_list[i][3])
            z_s.append(properties_list[i][4])
            z_p.append(properties_list[i][5])

binwidth = 0.01

fig = plt.figure()

(n1, bins1, patches1) = plt.hist(z_s, bins=np.arange(0., max(z_s) + binwidth, binwidth), facecolor = 'red')

plt.title('Histogram of spectroscopic redshift - all - ' + sys.argv[1])
plt.xlabel('spectroscopic z')
plt.ylabel('n(z)')

plt.tight_layout()
plt.grid(True)
plt.show()

fig.savefig('../Plots/pdf/histograms/hist_z_spec_all_'+sys.argv[1]+'_'+sys.argv[2]+'_5.pdf')
fig.savefig('../Plots/png/histograms/hist_z_spec_all_'+sys.argv[1]+'_'+sys.argv[2]+'_5.png')

nofz_spec_all = open('../Sim_Data/nofz/nofz_spec_all_'+sys.argv[1]+'_'+sys.argv[2]+'_5.dat', 'w')
nofz_spec_all.write('# hist\n')

for n in range(0, len(n1)):
	if n < len(n1) - 1:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]) + '\n')
	else:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]))

nofz_spec_all.close()

#data = ascii.read('../Sim_Data/reduced/cen_reduced_'+sys.argv[1]+'_'+sys.argv[2]+'.asc', delimiter=' ' )

#z_s_cen = data['z_s']
        
#fig = plt.figure()

#(n2, bins2, patches2) = plt.hist(z_s_cen, bins=np.arange(0., max(z_s_cen) + binwidth, binwidth), facecolor = 'red')

#plt.title('Histogram of spectroscopic redshift - centrals - '+sys.argv[1])
#plt.xlabel('spectroscopic z')
#plt.ylabel('n(z)')

#plt.tight_layout()
#plt.grid(True)

#plt.show()

#fig.savefig('../Plots/pdf/histograms/hist_z_spec_cen_'+sys.argv[1]+'_'+sys.argv[2]+'.pdf')
#fig.savefig('../Plots/png/histograms/hist_z_spec_cen_'+sys.argv[1]+'_'+sys.argv[2]+'.png')

#nofz_spec_cen = open('../Sim_Data/nofz/nofz_spec_cen_'+sys.argv[1]+'_'+sys.argv[2]+'.dat', 'w')
#nofz_spec_cen.write('# hist\n')

#for n in range(0, len(n2)):
#	if n < len(n2) - 1:
#		nofz_spec_cen.write(str(bins2[n]) + ' ' +str(n2[n]) + '\n')
#	else:
#		nofz_spec_cen.write(str(bins2[n]) + ' ' +str(n2[n]))

#nofz_spec_cen.close()

#data = ascii.read('../Sim_Data/reduced/sat_reduced_'+sys.argv[1]+'_'+sys.argv[2]+'.asc', delimiter=' ' )

#z_s_sat = data['z_s']

#fig = plt.figure()

#(n3, bins3, patches3) = plt.hist(z_s_sat, bins=np.arange(0., max(z_s_sat) + binwidth, binwidth), facecolor = 'red')

#plt.title('Histogram of spectroscopic redshift - satellites - ' + sys.argv[1])
#plt.xlabel('spectroscopic z')
#plt.ylabel('n(z)')

#plt.tight_layout()
#plt.grid(True)

#plt.show()

#fig.savefig('../Plots/pdf/histograms/hist_z_spec_sat_'+sys.argv[1]+'_'+sys.argv[2]+'.pdf')
#fig.savefig('../Plots/png/histograms/hist_z_spec_sat_'+sys.argv[1]+'_'+sys.argv[2]+'.png')

#nofz_spec_sat = open('../Sim_Data/nofz/nofz_spec_sat_'+sys.argv[1]+'_'+sys.argv[2]+'.dat', 'w')
#nofz_spec_sat.write('# hist\n')

#for n in range(0, len(n3)):
#	if n < len(n3) - 1:
#		nofz_spec_sat.write(str(bins3[n]) + ' ' +str(n3[n]) + '\n')
#	else:
#		nofz_spec_sat.write(str(bins3[n]) + ' ' +str(n3[n]))

#nofz_spec_sat.close()






