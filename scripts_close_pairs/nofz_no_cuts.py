from astropy.io import fits, ascii
import matplotlib.pyplot as plt
import numpy as np
import sys

# lines of sight = sys.argv[1]

hdulist = fits.open('../Sim_Data/catalogs/FullGalCatalogSDSS_zmax_0.3new_LOS'+sys.argv[1]+'.fits')
tbdata = hdulist[1].data

z_spec_all = tbdata.field(3)

binwidth = 0.01

fig = plt.figure()

(n1, bins1, patches1) = plt.hist(z_spec_all, bins=np.arange(0., max(z_spec_all) + binwidth, binwidth), facecolor = 'red')

plt.title('Histogram of spectroscopic redshift - all')
plt.xlabel('spectroscopic z')
plt.ylabel('n(z)')

plt.tight_layout()
plt.grid(True)

plt.show()

fig.savefig('../Plots/pdf/hist_z_spec_all_'+sys.argv[1]+'_none.pdf')
fig.savefig('../Plots/png/hist_z_spec_all_'+sys.argv[1]+'_none.png')

nofz_spec_all = open('../Sim_Data/nofz/nofz_spec_all_'+sys.argv[1]+'_none.dat', 'w')
nofz_spec_all.write('# hist\n')

for n in range(0, len(n1)):
	if n < len(n1) - 1:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]) + '\n')
	else:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]))

nofz_spec_all.close()

data = ascii.read('../Sim_Data/cen_sat/cen_'+sys.argv[1]+'.asc', delimiter=' ' )

z_spec_cen = data['col7']

fig = plt.figure()

(n2, bins2, patches2) = plt.hist(z_spec_cen, bins=np.arange(0., max(z_spec_cen) + binwidth, binwidth), facecolor = 'red')

plt.title('Histogram of spectroscopic redshift - centrals')
plt.xlabel('spectroscopic z')
plt.ylabel('n(z)')

plt.tight_layout()
plt.grid(True)

plt.show()

fig.savefig('../Plots/pdf/hist_z_spec_cen_'+sys.argv[1]+'_none.pdf')
fig.savefig('../Plots/png/hist_z_spec_cen_'+sys.argv[1]+'_none.png')

nofz_spec_cen = open('../Sim_Data/nofz/nofz_spec_cen_'+sys.argv[1]+'_none.dat', 'w')
nofz_spec_cen.write('# hist\n')

for n in range(0, len(n2)):
	if n < len(n2) - 1:
		nofz_spec_cen.write(str(bins2[n]) + ' ' +str(n2[n]) + '\n')
	else:
		nofz_spec_cen.write(str(bins2[n]) + ' ' +str(n2[n]))

nofz_spec_cen.close()

data = ascii.read('../Sim_Data/cen_sat/sat_'+sys.argv[1]+'.asc', delimiter=' ' )

z_spec_sat = data['col7']

fig = plt.figure()

(n3, bins3, patches3) = plt.hist(z_spec_sat, bins=np.arange(0., max(z_spec_sat) + binwidth, binwidth), facecolor = 'red')

plt.title('Histogram of spectroscopic redshift - satellites')
plt.xlabel('spectroscopic z')
plt.ylabel('n(z)')

plt.tight_layout()
plt.grid(True)

plt.show()

fig.savefig('../Plots/pdf/hist_z_spec_sat_'+sys.argv[1]+'_none.pdf')
fig.savefig('../Plots/png/hist_z_spec_sat_'+sys.argv[1]+'_none.png')

nofz_spec_sat = open('../Sim_Data/nofz/nofz_spec_sat_'+sys.argv[1]+'_none.dat', 'w')
nofz_spec_sat.write('# hist\n')

for n in range(0, len(n3)):
	if n < len(n3) - 1:
		nofz_spec_sat.write(str(bins3[n]) + ' ' +str(n3[n]) + '\n')
	else:
		nofz_spec_sat.write(str(bins3[n]) + ' ' +str(n3[n]))

nofz_spec_sat.close()






