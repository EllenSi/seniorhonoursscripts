from astropy.io import fits, ascii
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

dct_z_s_both = {}
dct_z_s_faint = {}
dct_z_s_random = {}


for h in ['both','faint','random']:
    for i in range(74,100):
        with open('../Sim_Data/reduced/all_reduced_'+h+'_'+str(i)+'.asc') as f:            data = f.read()

        print h,i
        data = data.split('\n')
        data = data[1:-1]

        data_list = []
        properties_list = []

        for j in data:
            data_list.append(j.split(' '))

        z_s = []

        for k in range(0, len(data_list)):
            properties_list.append([float(l) for l in data_list[k]])

        for m in range(0,len(properties_list)):
            z_s.append(properties_list[m][4])

        if h == 'both':
            dct_z_s_both[i-74] = z_s

        elif h =='faint':
            dct_z_s_faint[i-74] = z_s

        else:
            dct_z_s_random[i-74] = z_s

        print h,i

binwidth = 0.01        
fig = plt.figure()

for i in range(0,len(dct_z_s_both)):
    
    plt.hist(dct_z_s_both[i], bins=np.arange(0., max(dct_z_s_both[0]) + binwidth, binwidth), alpha = 0.5, facecolor = 'red')
    plt.hist(dct_z_s_faint[i], bins=np.arange(0., max(dct_z_s_faint[0]) + binwidth, binwidth), ls = 'dashed', alpha = 0.5, facecolor = 'green')
    plt.hist(dct_z_s_random[i], bins=np.arange(0., max(dct_z_s_random[0]) + binwidth, binwidth), ls = 'dotted', alpha = 0.5, facecolor = 'blue')
                                                                                  
fig.savefig('../test_hist.png')
