from astropy.io import ascii, fits
import random
import numpy as np
from astropy.table import Table, table
import fitsio
from fitsio import FITS,FITSHDR
import sys

# lines of sight = sys.argv[1]

with open('../Sim_Data/close_pairs/close_pairs_all_15_'+sys.argv[1]+'.txt') as f:
    data = f.read()

f.close()

data = data.split('\n')

gal1_array = []
gal2_array = []

for row in data[:-1]:
    gal_one, gal_two = (float(x.strip("(),")) for x in row.split())
    gal1_array.append(gal_one)
    gal2_array.append(gal_two)

hdulist = fits.open('../Sim_Data/catalogs/FullGalCatalogSDSS_zmax_0.3new_LOS'+sys.argv[1]+'.fits')
tbdata = hdulist[1].data

x = tbdata.field(1)
y = tbdata.field(2)
z_s = tbdata.field(3)
mag = tbdata.field(4)
s1 = tbdata.field(9)
s2 = tbdata.field(10)
z_p = tbdata.field(16) 

print(len(gal1_array))

mask = np.ones(len(tbdata), dtype=bool)

print(len(gal1_array))
for i in range(0,len(gal1_array)):

    if i%1000 == 0:
        frac = float(i)/float(len(gal1_array))
        print(i, frac)

    gal1 = int(gal1_array[i])
    gal2 = int(gal2_array[i])

    if mask[gal1] == True and mask[gal2] == True:
        
        if mag[gal1] > mag[gal2]:
            
            mask[gal1] = False
            
        else:
            mask[gal2] = False

tbdata2 = tbdata[mask,...]

x = tbdata2.field(1)
y = tbdata2.field(2)
s1 = tbdata2.field(9)
s2 = tbdata2.field(10)
z_s = tbdata2.field(3)
z_p = tbdata2.field(16)

print('writing the tables...')
                    
ascii.write([x_red, y_red,s1_red,s2_red, z_s_red,z_p_red], 'all_reduced_faint_'+sys.argv[1]+'.asc', names=['x','y','s1', 's2','z_s','z_p'])

new_col1 = fits.ColDefs([fits.Column(name='x_arcmin', format='D',array=np.array(x_red))])
new_col2 = fits.ColDefs([fits.Column(name='y_arcmin', format='D',array=np.array(y_red))])
new_col3 = fits.ColDefs([fits.Column(name='shear1', format='D',array=np.array(s1_red))])
new_col4 = fits.ColDefs([fits.Column(name='shear2', format='D',array=np.array(s2_red))])
hdu = fits.BinTableHDU.from_columns(new_col1 + new_col2 + new_col3 + new_col4)
hdu.writeto('../Sim_Data/reduced/all_reduced_faint_'+sys.argv[1]+'.fits')


        
