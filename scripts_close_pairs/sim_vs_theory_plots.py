import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.io import ascii

import sys

# galtype = sys.argv[1]
# cuts = sys.argv[2]
# lines of sight = sys.argv[3]

data = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.txt', delimiter=' ' )

theta1 = data['col1']
xi_m = data['col2']

data = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.txt', delimiter=' ' )

theta2 = data['col1']
xi_p = data['col2']

hdulist = fits.open('../Sim_Data/athena_data/xi_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.fits')
tbdata = hdulist[1].data

theta_array = tbdata.field(0)
xip_array = tbdata.field(1)
xim_array = tbdata.field(2)

fig = plt.figure()

plt.loglog(theta1, xi_m, 'b', theta_array, xim_array, 'bo')

plt.xlabel(r'$\theta$ [arcmin]')
plt.ylabel(r'$\xi_-$')

plt.title(r'$\xi_-$ vs. $\theta$ - '+sys.argv[1]+' - '+sys.argv[2])

plt.legend(['nicaea', 'athena'], loc='best')

plt.xlim( min(theta_array), max(theta1))

plt.axvline(x=0.5, linewidth=1, linestyle='dashed', color='k')

plt.tight_layout()

plt.grid(True)
plt.show()

fig.savefig('../Plots/pdf/nic_vs_ath/xi_m_nic_vs_ath_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.pdf')
fig.savefig('../Plots/png/nic_vs_ath/xi_m_nic_vs_ath_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.png')

fig = plt.figure()

plt.loglog(theta2, xi_p, 'b',theta_array, xip_array, 'bo')

plt.xlabel(r'$\theta$ [arcmin]')
plt.ylabel(r'$\xi_+$')

plt.title(r'$\xi_+$ vs. $\theta$ - '+sys.argv[1]+' - '+sys.argv[2])

plt.legend(['nicaea', 'athena'], loc='best')

plt.xlim( min(theta_array), max(theta2))

plt.axvline(x=0.5, linewidth=1, linestyle='dashed', color='k')

plt.tight_layout()

plt.grid(True)
plt.show()

fig.savefig('../Plots/pdf/nic_vs_ath/xi_p_nic_vs_ath_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.pdf')
fig.savefig('../Plots/png/nic_vs_ath/xi_p_nic_vs_ath_'+sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'.png')
