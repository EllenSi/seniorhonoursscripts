from astropy.io import ascii, fits
from astropy.table import Table, table
import fitsio
from fitsio import FITS,FITSHDR
import numpy as np
import sys
import time

from scipy import stats

with open('../Sim_Data/close_pairs/close_pairs_all_5_'+sys.argv[1]+'.txt') as f:
    data = f.read()

data = data.split('\n')

f.close()

gal_array = []

for row in data[:-1]:
    gal_one, gal_two = (float(x.strip("(),")) for x in row.split())
    gal_array.append(gal_one)
    gal_array.append(gal_two)
    
f.close()

gal_array = list(set(gal_array))

hdulist = fits.open('../Sim_Data/catalogs/FullGalCatalogSDSS_zmax_0.3new_LOS'+sys.argv[1]+'.fits')
tbdata  = hdulist[1].data

print(len(gal_array))

start_time = time.time()   

print tbdata.shape

mask = np.ones(len(tbdata), dtype=bool)
mask[gal_array] = False
tbdata2 = tbdata[mask,...]

x = tbdata2.field(1)
y = tbdata2.field(2)
s1 = tbdata2.field(9)
s2 = tbdata2.field(10)
z_s = tbdata2.field(3)
z_p = tbdata2.field(16)

print tbdata2.shape

print(time.time() - start_time)

print('writing the tables...')
                    
ascii.write([x, y,s1,s2, z_s,z_p], '../Sim_Data/reduced/all_reduced_both_'+sys.argv[1]+'.asc', names=['x','y','s1', 's2','z_s','z_p'])

new_col1 = fits.ColDefs([fits.Column(name='x_arcmin', format='D',array=np.array(x))])
new_col2 = fits.ColDefs([fits.Column(name='y_arcmin', format='D',array=np.array(y))])
new_col3 = fits.ColDefs([fits.Column(name='shear1', format='D',array=np.array(s1))])
new_col4 = fits.ColDefs([fits.Column(name='shear2', format='D',array=np.array(s2))])
hdu = fits.BinTableHDU.from_columns(new_col1 + new_col2 + new_col3 + new_col4)
hdu.writeto('../Sim_Data/reduced/all_reduced_both_'+sys.argv[1]+'.fits')
