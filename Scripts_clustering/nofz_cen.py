from astropy.io import fits, ascii
import matplotlib.pyplot as plt
import numpy as np
import sys

# lines of sight = sys.argv[1]



data = ascii.read('../Sim_Data/cen_sat/cen_fixseed_'+sys.argv[1]+'.asc')

z_spec_all = data['col7']

binwidth = 0.01

fig = plt.figure()

(n1, bins1, patches1) = plt.hist(z_spec_all, bins=np.arange(0., max(z_spec_all) + binwidth, binwidth), facecolor = 'red')

plt.title('Histogram of spectroscopic redshift - cen - fixseed')
plt.xlabel('spectroscopic z')
plt.ylabel('n(z)')

plt.tight_layout()
plt.grid(True)

plt.show()

fig.savefig('../Plots/png/hist_z_spec_cen_fix_'+sys.argv[1]+'.png')


nofz_spec_all = open('../Sim_Data/nofz/nofz_spec_cen_fix_'+sys.argv[1]+'.dat', 'w')
nofz_spec_all.write('# hist\n')

for n in range(0, len(n1)):
	if n < len(n1) - 1:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]) + '\n')
	else:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]))

nofz_spec_all.close()

data = ascii.read('../Sim_Data/cen_sat/cen_random_fixseed_'+sys.argv[1]+'.asc')

z_spec_all = data['col7']

binwidth = 0.01

fig = plt.figure()

(n1, bins1, patches1) = plt.hist(z_spec_all, bins=np.arange(0., max(z_spec_all) + binwidth, binwidth), facecolor = 'red')

plt.title('Histogram of spectroscopic redshift - cen - RANDOM fixseed')
plt.xlabel('spectroscopic z')
plt.ylabel('n(z)')

plt.tight_layout()
plt.grid(True)

plt.show()

fig.savefig('../Plots/png/hist_z_spec_cen_rand_'+sys.argv[1]+'.png')

nofz_spec_all = open('../Sim_Data/nofz/nofz_spec_cen_rand_'+sys.argv[1]+'.dat', 'w')
nofz_spec_all.write('# hist\n')

for n in range(0, len(n1)):
	if n < len(n1) - 1:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]) + '\n')
	else:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]))

nofz_spec_all.close()





