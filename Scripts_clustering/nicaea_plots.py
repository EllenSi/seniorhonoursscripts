import numpy as np
from astropy.io import fits, ascii
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
from astropy.table import Table
from scipy.interpolate import interp1d

th = []
xim_fix = []
xip_fix = []
xim_rand = []
xip_rand = []

for i in range(74,100):
    data1 = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_all_RANDOM_fixseed_'+str(i)+'.txt', delimiter=' ' )
    data2 = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_all_RANDOM_fixseed_'+str(i)+'.txt', delimiter=' ' )
    data3 = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_all_fixseed_'+str(i)+'.txt', delimiter=' ' )
    data4 = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_all_fixseed_'+str(i)+'.txt', delimiter=' ' )   

    theta1 = data1['col1']
    xi_m_rand = data1['col2']

    theta2 = data2['col1']
    xi_p_rand = data2['col2']

    theta3 = data3['col1']
    xi_m_fix = data3['col2']

    theta4 = data4['col1']
    xi_p_fix = data4['col2']

    th.append(theta1)
    xim_rand.append(xi_m_rand)
    xip_rand.append(xi_p_rand)
    xim_fix.append(xi_m_fix)
    xip_fix.append(xi_p_fix)

    
hdulist = fits.open('../Sim_Data/athena_data/xi_all_random_fixseed_75.fits')

tbdata = hdulist[1].data

theta_array = tbdata.field(0)
xip_array = tbdata.field(1)
xim_array = tbdata.field(2)

th_ath_short = []

for i in range(0, len(theta_array)):
    if theta_array[i] >= min(theta1) and  theta_array[i] <= max(theta1):
        th_ath_short.append(theta_array[i]) 

xim_nic_ath_fix = []
xip_nic_ath_fix = []
xim_nic_ath_rand = []
xip_nic_ath_rand = []

for i in range(0,len(th)):

    f_xi_m_nic_rand = interp1d(th[i], xim_rand[i])
    xim_nic_ath_rand.append(f_xi_m_nic_rand(th_ath_short))
    f_xi_p_nic_rand = interp1d(th[i], xip_rand[i])
    xip_nic_ath_rand.append(f_xi_p_nic_rand(th_ath_short))
    f_xi_m_nic_fix = interp1d(th[i], xim_fix[i])
    xim_nic_ath_fix.append(f_xi_m_nic_fix(th_ath_short))
    f_xi_p_nic_fix = interp1d(th[i], xip_fix[i])
    xip_nic_ath_fix.append(f_xi_p_nic_fix(th_ath_short))

dct_th = {}
dct_xi_p_rand = {}
dct_xi_m_rand = {}
dct_xi_p_fix = {}
dct_xi_m_fix = {}



for i in range(0,len(th_ath_short)):
    
    dct_xi_p_rand[i] =  []
    dct_xi_m_rand[i] = []
    dct_xi_p_fix[i] =  []
    dct_xi_m_fix[i] = []
    
    
for i in range(0,len(xip_nic_ath_fix)):
    for j in range(0,len(th_ath_short)):
        
        dct_xi_p_rand[j].append(xip_nic_ath_rand[i][j])
        dct_xi_m_rand[j].append(xim_nic_ath_rand[i][j])
        dct_xi_p_fix[j].append(xip_nic_ath_fix[i][j])
        dct_xi_m_fix[j].append(xim_nic_ath_fix[i][j])

mean_xip_nic_rand = []
mean_xim_nic_rand = []        
stdev_xip_nic_rand = []
stdev_xim_nic_rand = []

mean_xip_nic_fix = []
mean_xim_nic_fix = []        
stdev_xip_nic_fix = []
stdev_xim_nic_fix = []


for k in range(0,len(dct_xi_p_rand)):
    mean_xip_nic_rand.append(np.mean(np.array(dct_xi_p_rand[k])))
    mean_xim_nic_rand.append(np.mean(np.array(dct_xi_m_rand[k])))    
    stdev_xip_nic_rand.append(np.std(np.array(dct_xi_p_rand[k])))
    stdev_xim_nic_rand.append(np.std(np.array(dct_xi_m_rand[k])))
    
    mean_xip_nic_fix.append(np.mean(np.array(dct_xi_p_fix[k])))
    mean_xim_nic_fix.append(np.mean(np.array(dct_xi_m_fix[k])))    
    stdev_xip_nic_fix.append(np.std(np.array(dct_xi_p_fix[k])))
    stdev_xim_nic_fix.append(np.std(np.array(dct_xi_m_fix[k])))
    
err_mean_xip_nic_rand = []
err_mean_xim_nic_rand = []
err_mean_xip_nic_fix = []
err_mean_xim_nic_fix = []

for i in range(0,len(stdev_xim_nic_rand)):

    err_mean_xip_nic_rand.append(stdev_xip_nic_rand[i]/np.sqrt(len(stdev_xip_nic_rand)))
    err_mean_xim_nic_rand.append(stdev_xim_nic_rand[i]/np.sqrt(len(stdev_xim_nic_rand)))
    err_mean_xip_nic_fix.append(stdev_xip_nic_fix[i]/np.sqrt(len(stdev_xip_nic_fix)))
    err_mean_xim_nic_fix.append(stdev_xim_nic_fix[i]/np.sqrt(len(stdev_xim_nic_fix)))
    
print ' making plot...'
 
fig = plt.figure()

for i in range(0,len(th)):
    
    plt.loglog(th[i], xip_rand[i], '-',color='0.75', lw=0.25)

ax = fig.add_subplot(111)

ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')

plt.errorbar(th_ath_short, mean_xip_nic_rand, yerr=err_mean_xip_nic_rand, fmt='bo',ms=4)
plt.errorbar(th_ath_short, mean_xip_nic_rand, yerr=stdev_xip_nic_rand,fmt='ro',ms=4)
plt.xlim([min(theta1),max(theta1)])
plt.ylim([5e-8,3e-5])
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\xi_{+}^{nicaea}$', fontsize=20)
plt.title(r'$\xi_{+}$ vs. $\theta$ - random fixed seed, mean of LoS 74-99 ')

plt.tight_layout()  
plt.show()

fig.savefig('../Plots/png/nic/variation_xip_all_random_fixseed.png')

print ' making plot...'
 
fig = plt.figure()

for i in range(0,len(th)):
    
    plt.loglog(th[i], xim_rand[i], '-',color='0.75', lw=0.25)

ax = fig.add_subplot(111)

ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')

plt.errorbar(th_ath_short, mean_xim_nic_rand, yerr=err_mean_xim_nic_rand, fmt='bo',ms=4)
plt.errorbar(th_ath_short,mean_xim_nic_rand, yerr=stdev_xim_nic_rand, fmt='ro',ms=4)
plt.xlim([min(theta1),max(theta1)])
#plt.ylim([5e-8,2e-6])
plt.title(r'$\xi_{-}$ vs. $\theta$ - random fixed seed, mean of LoS 74-99 ')

plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\xi_{-}^{nicaea}$', fontsize=20)

plt.tight_layout()  
plt.show()

fig.savefig('../Plots/png/nic/variation_xim_all_random_fixseed.png')

print ' making plot...'
 
fig = plt.figure()

for i in range(0,len(th)):
    
    plt.loglog(th[i], xip_fix[i], '-',color='0.75', lw=0.25)

ax = fig.add_subplot(111)

ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')

plt.errorbar(th_ath_short, mean_xip_nic_fix, yerr=err_mean_xip_nic_fix, fmt='bo',ms=4)
plt.errorbar(th_ath_short, mean_xip_nic_fix, yerr=stdev_xip_nic_fix,fmt='ro',ms=4)
plt.xlim([min(theta1),max(theta1)])
#plt.ylim([5e-8,3e-5])
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\xi_{+}^{nicaea}$', fontsize=20)
plt.title(r'$\xi_{+}$ vs. $\theta$ - fixed seed, mean of LoS 74-99 ')
plt.tight_layout()  
plt.show()

fig.savefig('../Plots/png/nic/variation_xip_all_fixseed.png')

print ' making plot...'
 
fig = plt.figure()

for i in range(0,len(th)):
    
    plt.loglog(th[i], xim_fix[i], '-',color='0.75', lw=0.25)

ax = fig.add_subplot(111)

ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')

plt.errorbar(th_ath_short, mean_xim_nic_fix, yerr=err_mean_xim_nic_fix, fmt='bo',ms=4)
plt.errorbar(th_ath_short,mean_xim_nic_fix, yerr=stdev_xim_nic_fix, fmt='ro',ms=4)
plt.xlim([min(theta1),max(theta1)])
#plt.ylim([5e-8,2e-6])

plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\xi_{-}^{nicaea}$', fontsize=20)
plt.title(r'$\xi_{-}$ vs. $\theta$ - fixed seed, mean of LoS 74-99 ')
plt.tight_layout()  
plt.show()

fig.savefig('../Plots/png/nic/variation_xim_all_fixseed.png')

data = Table([th_ath_short, mean_xip_nic_rand, mean_xim_nic_rand, err_mean_xip_nic_rand, err_mean_xim_nic_rand,stdev_xip_nic_rand,stdev_xim_nic_rand], names=[ 'th_nic_rand', 'mean_xip_nic_rand','mean_xim_nic_rand','mean_err_xip_nic_rand','mean_err_xim_nic_rand','stdev_xip_nic_rand','stdev_xim_nic_rand'])
ascii.write(data, '../Sim_Data/stdev/mean_nic_all_random_fixseed.txt')

data = Table([th_ath_short, mean_xip_nic_fix, mean_xim_nic_fix, err_mean_xip_nic_fix, err_mean_xim_nic_fix,stdev_xip_nic_fix,stdev_xim_nic_fix], names=[ 'th_nic_fix', 'mean_xip_nic_fix','mean_xim_nic_fix','mean_err_xip_nic_fix','mean_err_xim_nic_fix','stdev_xip_nic_fix','stdev_xim_nic_fix'])
ascii.write(data, '../Sim_Data/stdev/mean_nic_all_fixseed.txt')
