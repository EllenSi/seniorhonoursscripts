import sys

# LoS = sys.argv[1]

# opens and reads file with test data
with open('../Sim_Data/nicaea_data/xi_m_all_fix_'+sys.argv[1]) as f:
    data = f.read()

# splits data into seperate rows
data = data.split('\n')

index = 0

for i in range(0, len(data)):
	if data[i] == '# th[\']             00':
		index = i + 1

xi_m_theory = open('../Sim_Data/nicaea_data/xi_m_nic_all_fixseed_'+sys.argv[1]+'.txt', 'w')

for i in range(index, len(data)):
	if i < len(data) - 1:
		xi_m_theory.write(data[i].lstrip('  ') + '\n')
	else:
		xi_m_theory.write(data[i].lstrip('  '))

xi_m_theory.close()
f.close()

# opens and reads file with test data
with open('../Sim_Data/nicaea_data/xi_p_all_fix_'+sys.argv[1]) as f:
    data = f.read()

# splits data into seperate rows
data = data.split('\n')

index = 0

for i in range(0, len(data)):
	if data[i] == '# th[\']             00':
		index = i + 1

xi_p_theory = open('../Sim_Data/nicaea_data/xi_p_nic_all_fixseed_'+sys.argv[1]+'.txt','w')

for i in range(index, len(data)):
	if i < len(data) - 1:
		xi_p_theory.write(data[i].lstrip('  ') + '\n')
	else:
		xi_p_theory.write(data[i].lstrip('  '))

xi_p_theory.close()
f.close()
# opens and reads file with test data
with open('../Sim_Data/nicaea_data/xi_m_all_rand_'+sys.argv[1]) as f:
    data = f.read()

# splits data into seperate rows
data = data.split('\n')

index = 0

for i in range(0, len(data)):
	if data[i] == '# th[\']             00':
		index = i + 1

xi_m_theory = open('../Sim_Data/nicaea_data/xi_m_nic_all_RANDOM_fixseed_'+sys.argv[1]+'.txt', 'w')

for i in range(index, len(data)):
	if i < len(data) - 1:
		xi_m_theory.write(data[i].lstrip('  ') + '\n')
	else:
		xi_m_theory.write(data[i].lstrip('  '))

xi_m_theory.close()
f.close()

# opens and reads file with test data
with open('../Sim_Data/nicaea_data/xi_p_all_rand_'+sys.argv[1]) as f:
    data = f.read()

# splits data into seperate rows
data = data.split('\n')

index = 0

for i in range(0, len(data)):
	if data[i] == '# th[\']             00':
		index = i + 1

xi_p_theory = open('../Sim_Data/nicaea_data/xi_p_nic_all_RANDOM_fixseed_'+sys.argv[1]+'.txt','w')

for i in range(index, len(data)):
	if i < len(data) - 1:
		xi_p_theory.write(data[i].lstrip('  ') + '\n')
	else:
		xi_p_theory.write(data[i].lstrip('  '))

xi_p_theory.close()
f.close()
