from astropy.io import fits, ascii
import matplotlib.pyplot as plt
import numpy as np
import sys

# lines of sight = sys.argv[1]



hdulist = fits.open('../Sim_Data/catalogs/FullGalCatalogSDSS_zmax_0.3new_fixseed_LOS'+sys.argv[1]+'.fits')
tbdata = hdulist[1].data

z_spec_all = tbdata.field(3)

binwidth = 0.01

fig = plt.figure()

(n1, bins1, patches1) = plt.hist(z_spec_all, bins=np.arange(0., max(z_spec_all) + binwidth, binwidth), facecolor = 'red')

plt.title('Histogram of spectroscopic redshift - all - fixseed')
plt.xlabel('spectroscopic z')
plt.ylabel('n(z)')

plt.tight_layout()
plt.grid(True)

plt.show()

fig.savefig('../Plots/png/hist_z_spec_all_fix_'+sys.argv[1]+'.png')


nofz_spec_all = open('../Sim_Data/nofz/nofz_spec_all_fix_'+sys.argv[1]+'.dat', 'w')
nofz_spec_all.write('# hist\n')

for n in range(0, len(n1)):
	if n < len(n1) - 1:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]) + '\n')
	else:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]))

nofz_spec_all.close()


hdulist = fits.open('../Sim_Data/catalogs/FullGalCatalogSDSS_zmax_0.3new_RANDOM_fixseed_LOS'+sys.argv[1]+'.fits')
tbdata = hdulist[1].data

z_spec_all = tbdata.field(3)

binwidth = 0.01

fig = plt.figure()

(n1, bins1, patches1) = plt.hist(z_spec_all, bins=np.arange(0., max(z_spec_all) + binwidth, binwidth), facecolor = 'red')

plt.title('Histogram of spectroscopic redshift - all - RANDOM fixseed')
plt.xlabel('spectroscopic z')
plt.ylabel('n(z)')

plt.tight_layout()
plt.grid(True)

plt.show()

fig.savefig('../Plots/png/hist_z_spec_all_rand_'+sys.argv[1]+'.png')

nofz_spec_all = open('../Sim_Data/nofz/nofz_spec_all_rand_'+sys.argv[1]+'.dat', 'w')
nofz_spec_all.write('# hist\n')

for n in range(0, len(n1)):
	if n < len(n1) - 1:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]) + '\n')
	else:
		nofz_spec_all.write(str(bins1[n]) + ' ' +str(n1[n]))

nofz_spec_all.close()





