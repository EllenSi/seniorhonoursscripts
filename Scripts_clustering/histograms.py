from astropy.io import fits, ascii
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

dct_z_s_unclus = {}
dct_z_s_clus  = {}

for h in ['RANDOM_fixseed','fixseed']:
    for i in range(74,100):
        hdulist = fits.open('../Sim_Data/catalogs/FullGalCatalogSDSS_zmax_0.3new_'+h+'_LOS'+str(i)+'.fits')           
#        print h,i

        tbdata = hdulist[1].data
        z_s = tbdata.field(3)

        if h == 'RANDOM_fixseed':
            dct_z_s_unclus[i-74] = z_s

        else:
            dct_z_s_clus[i-74] = z_s

        print h,i

binwidth = 0.01        
fig = plt.figure()

means = []
stdevs = []
n = []

for i in range(0,len(dct_z_s_clus)):
    print i
    n1, bins1, k1 = plt.hist(dct_z_s_clus[i],bins=np.arange(0., max(dct_z_s_clus[0]) + binwidth, binwidth) , lw=2, facecolor='None', edgecolor='black')
    #n2, bins2, k2 = plt.hist(dct_z_s_unclus[i], bins=np.arange(0., max(dct_z_s_unclus[0]) + binwidth, binwidth), lw=2,fc='None', edgecolor='grey')

    bincenters1 = 0.5*(bins1[1:]+bins1[:-1])
    #bincenters2 = 0.5*(bins2[1:]+bins2[:-1])

    #plt.plot(bincenters1,n1,'-')
    #plt.plot(bincenters2,n2,'-')

    n.append(n1)
    #n.append(n2)
                                                         
#fig.savefig('../test_hist_3.png')
maxs = []
mins = []

for i in range(0,len(n[0])):
    print i
    means.append(np.mean([n[j][i] for j in range(0,len(n))]))
    stdevs.append(np.std([n[j][i] for j in range(0,len(n))]))


for j in range(0,len(n[0])):
    mx = 0.
    mn = np.inf
    for i in range(0,len(n)):
        if n[i][j] > mx:
            mx = n[i][j]
        elif n[i][j] < mn:
            mn = n[i][j]
        print mx, mn
       
    maxs.append(mx)
    mins.append(mn)

min_error = [abs(means[i]-mins[i]) for i in range(0,len(means))]
plus_error = [abs(maxs[i] - means[i]) for i in range(0,len(means))]                
fig = plt.figure()

#plt.errorbar(bincenters2,means, yerr=[min_error,plus_error],alpha=0.5,fmt='ko')

#plt.errorbar(bincenters1,means,yerr=stdevs,fmt='ro')
#plt.plot(bincenters1,maxs,c='grey',ms=2,marker='o')
#plt.plot(bincenters1,mins,c='grey',ms=2,marker='o')
#plt.fill_between(bincenters1,  np.array(means)-np.array(min_error), np.array(means)+np.array(plus_error), alpha=0.25, facecolor='grey',edgecolor='grey')
#plt.xlim([0.0,max(bincenters1)+0.005])
#plt.errorbar(bincenters2,means, yerr=stdevs, fmt='ro')
#plt.fill_between(bincenters2, np.array(means)-np.array(min_error), np.array(means)+np.array(plus_error), alpha=0.5, facecolor='grey',edgecolor='grey')
#plt.tight_layout()
#plt.xlabel('Spectroscopic Redshift')
#plt.ylabel('Number of Galaxies')

fig.savefig('../hist_means_clustered.png')

distr_p = [[bincenters1[i] for j in range(0,int(maxs[i]))] for i in range(0,len(maxs))]

flat_p = [val for sublist in distr_p for val in sublist]

distr_m = [[bincenters1[i] for j in range(0,int(mins[i]))] for i in range(0,len(mins))]

flat_m = [val for sublist in distr_m for val in sublist]

fig = plt.figure()
#plt.hist(flat_p,bins=np.arange(0., max(dct_z_s_clus[0]) + binwidth, binwidth),facecolor=(0,0,0,0.2),edgecolor=(0,0,0,0.2))
#plt.hist(flat_m,bins=np.arange(0., max(dct_z_s_clus[0]) + binwidth, binwidth),facecolor='white',edgecolor='white')
plt.errorbar(bincenters1,means,yerr=stdevs,fmt='ro')
#plt.xlim([0.0,max(dct_z_s_clus[0])+0.005])
plt.xlabel('Spectroscopic Redshift',fontsize=20)
plt.ylabel('Number of Galaxies',fontsize=20)
fig.savefig('../means_test.png')
