from astropy.table import Table
import fitsio
from fitsio import FITS,FITSHDR
import numpy as np
from astropy.io import fits, ascii
from astropy.table import table
import sys

# galtype = sys.argv[1]
# lines of sight = sys.argv[2]

with open('../Sim_Data/cen_sat/'+sys.argv[1]+sys.argv[2]+'fixseed_'+sys.argv[3]+'.asc') as f:
    data = f.read()

data = data.split('\n')
data_new = data[1:-1]

data_list = []
properties_list = []

print(len(data_new))

for i in data_new:
    data_list.append(i.split(' '))

x = []
y = []
s1 = []
s2 = []

for j in range(0, len(data_list)):
    properties_list.append([float(l) for l in data_list[j]])

for i in range(0,len(properties_list)):
    
            x.append(properties_list[i][0])     
            y.append(properties_list[i][1])
            s1.append(properties_list[i][3])
            s2.append(properties_list[i][4])


            
new_col1 = fits.ColDefs([fits.Column(name='x_arcmin', format='D',array=np.array(x))])
new_col2 = fits.ColDefs([fits.Column(name='y_arcmin', format='D',array=np.array(y))])
new_col3 = fits.ColDefs([fits.Column(name='shear1', format='D',array=np.array(s1))])
new_col4 = fits.ColDefs([fits.Column(name='shear2', format='D',array=np.array(s2))])
hdu = fits.BinTableHDU.from_columns(new_col1 + new_col2 + new_col3 + new_col4)
hdu.writeto('../Sim_Data/cen_sat/'+sys.argv[1]+sys.argv[2]+'fixseed_'+sys.argv[3]+'.fits')
