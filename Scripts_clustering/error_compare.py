import numpy as np
from astropy.io import fits, ascii
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
from astropy.table import Table

data = ascii.read('../Sim_Data/stdev/mean_ath_all_random_fixseed.txt')

th1_rand = list(data['th_ath_rand'])
mn_err_xip_rand = data['mean_err_xip_rand']
mr_err_xim_rand = data['mean_err_xim_rand']
std_xip_rand = data['stdev_xip_rand']
std_xim_rand = data['stdev_xim_rand']

data = ascii.read('../Sim_Data/stdev/mean_nic_all_random_fixseed.txt')

th2_rand = list(data['th_nic_rand'])
mn_err_xip_nic_rand = data['mean_err_xip_nic_rand']
mr_err_xim_nic_rand = data['mean_err_xim_nic_rand']
std_xip_nic_rand = data['stdev_xip_nic_rand']
std_xim_nic_rand = data['stdev_xim_nic_rand']

data = ascii.read('../Sim_Data/stdev/mean_ath_all_fixseed.txt')

th1_fix = list(data['th_fix'])
mn_err_xip_fix = data['mean_err_xip_fix']
mr_err_xim_fix = data['mean_err_xim_fix']
std_xip_fix = data['stdev_xip_fix']
std_xim_fix = data['stdev_xim_fix']

data = ascii.read('../Sim_Data/stdev/mean_nic_all_fixseed.txt')

th2_fix = list(data['th_nic_fix'])
mn_err_xip_nic_fix = data['mean_err_xip_nic_fix']
mr_err_xim_nic_fix = data['mean_err_xim_nic_fix']
std_xip_nic_fix = data['stdev_xip_nic_fix']
std_xim_nic_fix = data['stdev_xim_nic_fix']

frac_std_xip_rand = []
frac_std_xim_rand = []
frac_std_xip_fix = []
frac_std_xim_fix = []

for i in th2_rand:
    frac_std_xip_rand.append(std_xip_nic_rand[th2_rand.index(i)]/std_xip_rand[th1.index(i)])
    frac_std_xim_rand.append(std_xim_nic_rand[th2_rand.index(i)]/std_xim_rand[th1.index(i)])
    frac_std_xip_fix.append(std_xip_nic_fix[th2_fix.index(i)]/std_xip_fix[th1_fix.index(i)])
    frac_std_xim_fix.append(std_xim_nic_fix[th2_fix.index(i)]/std_xim_fix[th1_fix.index(i)])

fig = plt.figure()
plt.loglog(th2_rand, frac_std_xip_rand, 'ro')
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\frac{\sigma_{nicaea}}{\sigma_{athena}}$ $\xi^{+}$',fontsize=20)
plt.title(r'Ratio of std dev. of nicaea and athena data for $\xi^{+}$ vs. $\theta$ - random fixseed')
plt.tight_layout()
plt.show()
fig.savefig('../Plots/png/errors/ratio_err_na_xip_all_random_fixseed.png')


fig = plt.figure()
plt.loglog(th2_rand, frac_std_xim_rand,'ro')
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\frac{\sigma_{nicaea}}{\sigma_{athena}}$ $\xi^{-}$',fontsize=20)
plt.title(r'Ratio of std dev. of nicaea and athena data for $\xi^{-}$ vs. $\theta$ - random fixseed')
plt.tight_layout()
plt.show()

fig.savefig('../Plots/png/errors/ratio_err_na_xim_all_random_fixseed.png')


fig = plt.figure()
plt.loglog(th2_fix, frac_std_xip_fix, 'ro')
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\frac{\sigma_{nicaea}}{\sigma_{athena}}$ $\xi^{+}$',fontsize=20)
plt.title(r'Ratio of std dev. of nicaea and athena data for $\xi^{+}$ vs. $\theta$ - fixseed')
plt.tight_layout()
plt.show()
fig.savefig('../Plots/png/errors/ratio_err_na_xip_all_fixseed.png')


fig = plt.figure()
plt.loglog(th2_fix, frac_std_xim_fix,'ro')
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\frac{\sigma_{nicaea}}{\sigma_{athena}}$ $\xi^{-}$',fontsize=20)
plt.title(r'Ratio of std dev. of nicaea and athena data for $\xi^{-}$ vs. $\theta$ - fixseed')
plt.tight_layout()
plt.show()

fig.savefig('../Plots/png/errors/ratio_err_na_xim_all_fixseed.png')
