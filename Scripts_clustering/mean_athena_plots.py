import numpy as np
from astropy.io import fits, ascii
#import matplotlib
#matplotlib.use('Agg')

import matplotlib.pyplot as plt
from astropy.table import Table
from astropy.table import Table
from scipy.interpolate import interp1d

dct_t_rand = {}
dct_xip_rand = {}
dct_xim_rand = {}

dct_t_fix = {}
dct_xip_fix = {}
dct_xim_fix = {}

for i in range(74,100):
     
    hdulist_rand = fits.open('../Sim_Data/athena_data/xi_all_random_fixseed_'+str(i)+'.fits')
    tbdata_rand = hdulist_rand[1].data
    th_rand = tbdata_rand.field(0)
    xip_rand = tbdata_rand.field(1)
    xim_rand = tbdata_rand.field(2)

    if i == 74:
        for j in range(0,len(th_rand)):
            dct_t_rand[str(j)] = []
            dct_xip_rand[str(j)] =  []
            dct_xim_rand[str(j)] = []

    for j in range(0,len(th_rand)):
        
        dct_t_rand[str(j)].append(tbdata_rand.field(0)[j])
        dct_xip_rand[str(j)].append(tbdata_rand.field(1)[j])
        dct_xim_rand[str(j)].append(tbdata_rand.field(2)[j])
        
    hdulist_fix = fits.open('../Sim_Data/athena_data/xi_all_fixseed_'+str(i)+'.fits')
    tbdata_fix = hdulist_fix[1].data
    th_fix = tbdata_fix.field(0)
    xip_fix = tbdata_fix.field(1)
    xim_fix = tbdata_fix.field(2)

    if i == 74:
        for j in range(0,len(th_fix)):
            dct_t_fix[str(j)] = []
            dct_xip_fix[str(j)] =  []
            dct_xim_fix[str(j)] = []

    for j in range(0,len(th_fix)):
        
        dct_t_fix[str(j)].append(tbdata_fix.field(0)[j])
        dct_xip_fix[str(j)].append(tbdata_fix.field(1)[j])
        dct_xim_fix[str(j)].append(tbdata_fix.field(2)[j])
    
mean_xip_rand = []
mean_xim_rand = []        
stdev_xip_rand = []
stdev_xim_rand = []

for k in range(0,len(dct_t_rand)):
    mean_xip_rand.append(np.mean(np.array(dct_xip_rand[str(k)])))
    mean_xim_rand.append(np.mean(np.array(dct_xim_rand[str(k)])))    
    stdev_xip_rand.append(np.std(np.array(dct_xip_rand[str(k)])))
    stdev_xim_rand.append(np.std(np.array(dct_xim_rand[str(k)])))

data_rand = Table([mean_xip_rand, mean_xim_rand, stdev_xip_rand, stdev_xim_rand], names=['mean_xip_rand', 'mean_xim_rand','std_xip_rand', 'std_xim_rand'])
ascii.write(data_rand, '../Sim_Data/stdev/mean_ath_all_random_fixseed.txt')
    
mean_xip_fix = []
mean_xim_fix = []        
stdev_xip_fix = []
stdev_xim_fix = []

for k in range(0,len(dct_t_fix)):
    mean_xip_fix.append(np.mean(np.array(dct_xip_fix[str(k)])))
    mean_xim_fix.append(np.mean(np.array(dct_xim_fix[str(k)])))    
    stdev_xip_fix.append(np.std(np.array(dct_xip_fix[str(k)])))
    stdev_xim_fix.append(np.std(np.array(dct_xim_fix[str(k)])))

data_fix = Table([mean_xip_fix, mean_xim_fix, stdev_xip_fix, stdev_xim_fix], names=['mean_xip_fix', 'mean_xim_fix','std_xip_fix', 'std_xim_fix'])
ascii.write(data_fix, '../Sim_Data/stdev/mean_ath_all_fixseed.txt')

th = []
xim_fix = []
xip_fix = []
xim_rand = []
xip_rand = []

for i in range(74,100):
    data1 = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_all_RANDOM_fixseed_'+str(i)+'.txt', delimiter=' ' )
    data2 = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_all_RANDOM_fixseed_'+str(i)+'.txt', delimiter=' ' )
    data3 = ascii.read('../Sim_Data/nicaea_data/xi_m_nic_all_fixseed_'+str(i)+'.txt', delimiter=' ' )
    data4 = ascii.read('../Sim_Data/nicaea_data/xi_p_nic_all_fixseed_'+str(i)+'.txt', delimiter=' ' )   

    theta1 = data1['col1']
    xi_m_rand = data1['col2']

    theta2 = data2['col1']
    xi_p_rand = data2['col2']

    theta3 = data3['col1']
    xi_m_fix = data3['col2']

    theta4 = data4['col1']
    xi_p_fix = data4['col2']

    th.append(theta1)
    xim_rand.append(xi_m_rand)
    xip_rand.append(xi_p_rand)
    xim_fix.append(xi_m_fix)
    xip_fix.append(xi_p_fix)

fig = plt.figure()
for i in range(0,len(th)):  
    plt.loglog(th[i], xip_fix[i], '-',color='0.75', lw=0.25)

ax = fig.add_subplot(111)
ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')
plt.errorbar(th_fix,mean_xip_fix, yerr=stdev_xip_fix, fmt='ro',ms=4)
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\xi_{+}$', fontsize=20)
plt.title(r'Mean $\xi_{+}$ of measurement and $\xi_{+}$ of predictions vs. $\theta$ - clustered')
plt.tight_layout() 
plt.show() 
fig.savefig('../Plots/png/ath+nic_xip_all_clustered.png')
       
fig = plt.figure()
for i in range(0,len(th)):
    plt.loglog(th[i], xim_fix[i], '-',color='0.75', lw=0.25)

ax = fig.add_subplot(111)
ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')
plt.errorbar(th_fix,mean_xim_fix, yerr=stdev_xim_fix, fmt='ro',ms=4)
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\xi_{-}$', fontsize=20)
plt.title(r'Mean $\xi_{-}$ of measurement and $\xi_{-}$ of predictions vs. $\theta$ - clustered')
plt.tight_layout()  
plt.show()
fig.savefig('../Plots/png/ath+nic_xim_all_clustered.png')


fig = plt.figure()
for i in range(0,len(th)):
    plt.loglog(th[i], xip_rand[i], '-',color='0.75', lw=0.25)

ax = fig.add_subplot(111)
ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')
plt.errorbar(th_rand,mean_xip_rand, yerr=stdev_xip_rand, fmt='ro',ms=4)
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\xi_{+}$', fontsize=20)
plt.title(r'Mean $\xi_{+}$ of measurement and $\xi_{+}$ of predictions vs. $\theta$ - random')
plt.tight_layout() 
plt.show() 
fig.savefig('../Plots/png/ath+nic_xip_all_unclustered.png')
    
fig = plt.figure()
for i in range(0,len(th)):
    plt.loglog(th[i], xim_rand[i], '-',color='0.75', lw=0.25)

ax = fig.add_subplot(111)
ax.set_xscale("log", nonposx='clip')
ax.set_yscale("log", nonposy='clip')
plt.errorbar(th_rand,mean_xim_rand, yerr=stdev_xim_rand, fmt='ro',ms=4)
plt.xlabel(r'$\theta$ (arcmin)')
plt.ylabel(r'$\xi_{-}$', fontsize=20)
plt.title(r'Mean $\xi_{-}$ of measurement and $\xi_{-}$ of predictions vs. $\theta$ - random')
plt.tight_layout()  
plt.show()
fig.savefig('../Plots/png/ath+nic_xim_all_unclustered.png')
    

    
    


