from astropy.io import ascii, fits
from astropy.table import Table, table
import fitsio
from fitsio import FITS,FITSHDR
import numpy as np
import sys
import time

from scipy import stats

hdulist = fits.open('../Sim_Data/catalogs/'+sys.argv[1]+'.fits')
tbdata  = hdulist[1].data

s1 = tbdata.field(9)
mask = np.ones(len(tbdata), dtype=bool)

for i in range(0,len(s1)):
    if np.isnan(s1[i]) == True:
        print(i)
        mask[i]  = False

tbdata2 = tbdata[mask,...]

x = tbdata2.field(1)
y = tbdata2.field(2)
s1 = tbdata2.field(9)
s2 = tbdata2.field(10)
z_s = tbdata2.field(3)
z_p = tbdata2.field(16)

print('writing the tables...')
                    
new_col1 = fits.ColDefs([fits.Column(name='x_arcmin', format='D',array=np.array(x))])
new_col2 = fits.ColDefs([fits.Column(name='y_arcmin', format='D',array=np.array(y))])
new_col3 = fits.ColDefs([fits.Column(name='shear1', format='D',array=np.array(s1))])
new_col4 = fits.ColDefs([fits.Column(name='shear2', format='D',array=np.array(s2))])
hdu = fits.BinTableHDU.from_columns(new_col1 + new_col2 + new_col3 + new_col4)
hdu.writeto('../Sim_Data/catalogs/'+sys.argv[1]+'_reduced.fits')
