import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.io import ascii
import sys

# galtype = sys.argv[1]

dct_th_fix = {}
dct_xip_fix = {}
dct_xim_fix = {}
dct_th_rand = {}
dct_xip_rand = {}
dct_xim_rand = {}
dct_th_fix_cen = {}
dct_xip_fix_cen = {}
dct_xim_fix_cen = {}
dct_th_rand_cen = {}
dct_xip_rand_cen = {}
dct_xim_rand_cen = {}

for i in range(0,15):
    dct_th_fix[i] = []
    dct_xip_fix[i] = []
    dct_xim_fix[i] = []
    dct_th_rand[i] = []
    dct_xip_rand[i] = []
    dct_xim_rand[i] = []
    dct_th_fix_cen[i] = []
    dct_xip_fix_cen[i] = []
    dct_xim_fix_cen[i] = []
    dct_th_rand_cen[i] = []
    dct_xip_rand_cen[i] = []
    dct_xim_rand_cen[i] = []

for i in range(74,100):

    hdulist = fits.open('../Sim_Data/athena_data/xi_all_fixseed_'+str(i)+'.fits')
    tbdata = hdulist[1].data

    th_fix = tbdata.field(0)
    xip_fix = tbdata.field(1)
    xim_fix = tbdata.field(2)

    hdulist = fits.open('../Sim_Data/athena_data/xi_all_random_fixseed_'+str(i)+'.fits')
    tbdata = hdulist[1].data

    th_rand = tbdata.field(0)
    xip_rand = tbdata.field(1)
    xim_rand = tbdata.field(2)

    hdulist = fits.open('../Sim_Data/athena_data/xi_cen_fixseed_'+str(i)+'.fits')
    tbdata = hdulist[1].data

    th_fix_cen = tbdata.field(0)
    xip_fix_cen = tbdata.field(1)
    xim_fix_cen = tbdata.field(2)

    hdulist = fits.open('../Sim_Data/athena_data/xi_cen_random_fixseed_'+str(i)+'.fits')
    tbdata = hdulist[1].data

    th_rand_cen = tbdata.field(0)
    xip_rand_cen = tbdata.field(1)
    xim_rand_cen = tbdata.field(2)

    for j in range(0,len(th_fix)):
        
        dct_th_fix[j].append(th_fix[j])
        dct_xip_fix[j].append(xip_fix[j])
        dct_xim_fix[j].append(xim_fix[j])
        dct_th_fix_cen[j].append(th_fix_cen[j])
        dct_xip_fix_cen[j].append(xip_fix_cen[j])
        dct_xim_fix_cen[j].append(xim_fix_cen[j])

    for j in range(0,len(th_rand)):
        
        dct_th_rand[j].append(th_rand[j])
        dct_xip_rand[j].append(xip_rand[j])
        dct_xim_rand[j].append(xim_rand[j])
        dct_th_rand_cen[j].append(th_rand_cen[j])
        dct_xip_rand_cen[j].append(xip_rand_cen[j])
        dct_xim_rand_cen[j].append(xim_rand_cen[j])

frac_xip = {}
frac_xim = {}

frac_xip_cen = {}
frac_xim_cen = {}

theta = []

for i in range(0,len(dct_th_fix)):

    frac_xip[i] = []
    frac_xim[i] = []
    frac_xip_cen[i] = []
    frac_xim_cen[i] = []

    rng = len(dct_th_fix[i])

    theta.append(dct_th_fix[i][0])
    
    for j in range(0,rng):
        
        fr_xip = (dct_xip_fix[i][j]/dct_xip_rand[i][j]) - 1.
        fr_xim = (dct_xim_fix[i][j]/dct_xim_rand[i][j]) - 1.

        frac_xip[i].append(fr_xip)
        frac_xim[i].append(fr_xim)
        
        fr_xip_cen = (dct_xip_fix_cen[i][j]/dct_xip_rand_cen[i][j]) - 1.
        fr_xim_cen = (dct_xim_fix_cen[i][j]/dct_xim_rand_cen[i][j]) - 1.

        frac_xip_cen[i].append(fr_xip_cen)
        frac_xim_cen[i].append(fr_xim_cen)

mean_fr_xip = []
mean_fr_xim = []

sig_fr_xip = []
sig_fr_xim = []

mean_fr_xip_cen = []
mean_fr_xim_cen = []

sig_fr_xip_cen = []
sig_fr_xim_cen = []

for i in range(0,len(dct_th_fix)):

    mean_fr_xip.append(np.mean(np.array(frac_xip[i])))
    mean_fr_xim.append(np.mean(np.array(frac_xim[i])))    
    sig_fr_xip.append(np.std(np.array(frac_xip[i]))/np.sqrt(len(frac_xip[i])))
    sig_fr_xim.append(np.std(np.array(frac_xim[i]))/np.sqrt(len(frac_xim[i])))
    mean_fr_xip_cen.append(np.mean(np.array(frac_xip_cen[i])))
    mean_fr_xim_cen.append(np.mean(np.array(frac_xim_cen[i])))    
    sig_fr_xip_cen.append(np.std(np.array(frac_xip_cen[i]))/np.sqrt(len(frac_xip_cen[i])))
    sig_fr_xim_cen.append(np.std(np.array(frac_xim_cen[i]))/np.sqrt(len(frac_xim_cen[i])))

fig = plt.figure()
plt.errorbar(theta,mean_fr_xip, yerr=sig_fr_xip, fmt='ro',label='All Galaxies')
plt.errorbar(theta,mean_fr_xip_cen, yerr=sig_fr_xip_cen, fmt='bo',label='Centrals')
plt.xscale('log')
plt.xlabel(r'$\theta$ (arcmin)',fontsize=20)
plt.xlim([1,max(theta)+20.])
plt.ylim([-0.2,0.4])
plt.ylabel(r'$\beta^{clustering}_{+}$',fontsize=25)
#plt.title(r'Ratio of $\xi_{+}$ for clustered/non clustered vs. $\theta$ - centrals')
plt.legend(loc='best')
plt.tight_layout()
plt.show()
fig.savefig('../Plots/png/frac_xip_clustered_all+cen_red.png')

fig = plt.figure()
plt.errorbar(theta,mean_fr_xim, yerr=sig_fr_xim, fmt='ro',label='All Galaxies')
plt.errorbar(theta,mean_fr_xim_cen, yerr=sig_fr_xim_cen, fmt='bo',label='Centrals')
plt.xscale('log')
plt.xlabel(r'$\theta$ (arcmin)',fontsize=20)
plt.xlim([1,max(theta)+20.])
plt.ylim([-0.2,1.5])
plt.ylabel(r'$\beta^{clustering}_{-}$',fontsize=25)
#plt.title(r'Ratio of $\xi_{-}$ for clustered/non clustered vs. $\theta$ - centrals')
plt.legend(loc='best')
plt.tight_layout()
plt.show()
fig.savefig('../Plots/png/frac_xim_clustered_all+cen_red.png')


